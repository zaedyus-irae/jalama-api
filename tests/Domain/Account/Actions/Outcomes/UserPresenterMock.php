<?php

namespace ZI\JalamaTests\Domain\Account\Actions\Outcomes;

use ZI\Jalama\Domain\Account\Actions\Outcomes\UserPresenter;
use ZI\JalamaTests\Domain\Shared\Actions\Outcomes\ResultsHolderMockTrait;

class UserPresenterMock implements UserPresenter
{
    use ResultsHolderMockTrait;
}
