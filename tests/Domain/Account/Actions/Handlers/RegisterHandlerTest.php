<?php

namespace ZI\JalamaTests\Domain\Account\Actions\Handlers;

use League\Tactician\CommandBus;
use ZI\Jalama\Domain\Account\Actions\Handlers\RegisterHandler;
use ZI\Jalama\Domain\Account\Actions\Register;
use ZI\Jalama\Domain\Account\Model\RepositoryInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\CreateShelf;
use ZI\JalamaTests\Domain\Account\Actions\Outcomes\UserPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class RegisterHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private RegisterHandler $handler;
    /** @var CommandBus|\PHPUnit\Framework\MockObject\MockObject */
    private CommandBus $commandBus;
    private UserPresenterMock $resultsHolder;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->commandBus = $this->createMock(CommandBus::class);
        $this->handler = new RegisterHandler($this->repository, 'test', $this->commandBus);
        $this->resultsHolder = new UserPresenterMock();
    }

    public function test can register(): void
    {
        $this->repository->expects($this->once())->method('saveUser')->withAnyParameters();
        $this->commandBus->expects($this->once())->method('handle')->with($this->isInstanceOf(CreateShelf::class));

        $moveGame = new Register(
            $this->resultsHolder,
            'test user',
            'test shelf'
        );

        $this->handler->handle($moveGame);

        $user = $this->resultsHolder->getResult();
        $this->assertInstanceOf(User::class, $user);
        $this->assertSame('test user', $user->getDisplayName());
    }
}
