<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Handlers\ListShelvesHandler;
use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedShelves;
use ZI\Jalama\Domain\Game\Actions\ListShelves;
use ZI\Jalama\Domain\Game\Model\Listing\ShelvesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\ShelvesSorts;
use ZI\Jalama\Domain\Game\Model\Listing\ShelvesSortType;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\Jalama\Domain\Game\Model\Shelves;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\ShelvesPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class ListShelvesHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private ShelvesSorts $sorts;
    private Pagination $pagination;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private ListShelvesHandler $handler;
    private ShelvesPresenterMock $shelvesPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->sorts = new ShelvesSorts(ShelvesSortType::NAME());
        $this->pagination = new Pagination(1, 1, 5);
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new ListShelvesHandler($this->repository);
        $this->shelvesPresenterMock = new ShelvesPresenterMock();
    }

    public function test can list shelves(): void
    {
        $shelves = new Shelves(
            Shelf::new($this->mainUser, 'shelf 1'),
            Shelf::new($this->mainUser, 'shelf 2'),
            Shelf::new($this->mainUser, 'shelf 3'),
            Shelf::new($this->mainUser, 'shelf 4'),
            Shelf::new($this->mainUser, 'shelf 5')
        );
        $this->repository->expects($this->once())->method('findShelves')->with($this->isInstanceOf(ShelvesFilters::class), $this->sorts, $this->pagination)->willReturn($shelves);
        $this->repository->expects($this->once())->method('countShelves')->with($this->isInstanceOf(ShelvesFilters::class))->willReturn(10);

        $listShelf = new ListShelves(
            $this->shelvesPresenterMock,
            $this->mainUser,
            $this->pagination,
            $this->sorts
        );

        $this->handler->handle($listShelf);

        $paginatedShelves = $this->shelvesPresenterMock->getResult();
        $this->assertInstanceOf(PaginatedShelves::class, $paginatedShelves);
        $this->assertSame($this->pagination, $paginatedShelves->getPagination());
        $this->assertSame(10, $paginatedShelves->getTotalNumberOfElements());
        $this->assertCount(5, $paginatedShelves);
        $this->assertSame($shelves[0], $paginatedShelves[0]);
        $this->assertSame($shelves[1], $paginatedShelves[1]);
        $this->assertSame($shelves[2], $paginatedShelves[2]);
        $this->assertSame($shelves[3], $paginatedShelves[3]);
        $this->assertSame($shelves[4], $paginatedShelves[4]);
    }
}
