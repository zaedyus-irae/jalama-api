<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\ViewGameHandler;
use ZI\Jalama\Domain\Game\Actions\ViewGame;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\GamePresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class ViewGameHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private User $otherUser;
    private Shelf $otherShelf;
    private Game $game;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private ViewGameHandler $handler;
    private GamePresenterMock $gamePresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->otherUser = $this->generateUser('manager');
        $this->otherShelf = Shelf::new($this->otherUser, 'test shelf');
        $this->game = Game::new($this->mainUser, $this->otherShelf, 'test shelf');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new ViewGameHandler($this->repository);
        $this->gamePresenterMock = new GamePresenterMock();
    }

    public function test can view a game as owner(): void
    {
        $this->repository->expects($this->once())->method('getGame')->with($this->game->getUuid())->willReturn($this->game);

        $viewGame = new ViewGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $this->game->getUuid()
        );

        $this->handler->handle($viewGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($this->game->getUuid(), $game->getUuid());
    }

    public function test can view a game as shelf user(): void
    {
        $this->repository->expects($this->once())->method('getGame')->with($this->game->getUuid())->willReturn($this->game);

        $viewGame = new ViewGame(
            $this->gamePresenterMock,
            $this->otherUser,
            $this->game->getUuid()
        );

        $this->handler->handle($viewGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($this->game->getUuid(), $game->getUuid());
    }

    public function test cannot view a game not owned in a shelf without access(): void
    {
        $this->repository->expects($this->once())->method('getGame')->with($this->game->getUuid())->willReturn($this->game);

        $viewGame = new ViewGame(
            $this->gamePresenterMock,
            $this->secondaryUser,
            $this->game->getUuid()
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Cannot view a game in a shelf you do not have access.');

        $this->handler->handle($viewGame);
    }

    public function test cannot view a not found game(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getGame')->with($uuid)->willReturn(null);

        $viewGame = new ViewGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $uuid
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked game.');

        $this->handler->handle($viewGame);
    }
}
