<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Account\Model\Users;
use ZI\Jalama\Domain\Game\Actions\DeleteShelf;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\IncorrectActionException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\DeleteShelfHandler;
use ZI\Jalama\Domain\Game\Model\Listing\GamesFilters;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class DeleteShelfHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private User $managerUser;
    private Shelf $shelf;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private DeleteShelfHandler $handler;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->managerUser = $this->generateUser('manager');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->shelf->setManagers(new Users($this->mainUser, $this->managerUser));
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new DeleteShelfHandler($this->repository);
    }

    public function test can remove own shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->once())->method('countGames')->with($this->isInstanceOf(GamesFilters::class))->willReturn(0);
        $this->repository->expects($this->once())->method('deleteShelf')->with($this->shelf->getUuid());

        $deleteShelf = new DeleteShelf($this->mainUser, $this->shelf->getUuid());

        $this->handler->handle($deleteShelf);
    }

    public function test cannot remove not owned shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('deleteShelf');

        $deleteShelf = new DeleteShelf($this->secondaryUser, $this->shelf->getUuid());

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Only the owner of a shelf can delete it.');

        $this->handler->handle($deleteShelf);
    }

    public function test cannot remove managed shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('deleteShelf');

        $deleteShelf = new DeleteShelf($this->managerUser, $this->shelf->getUuid());

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Only the owner of a shelf can delete it.');

        $this->handler->handle($deleteShelf);
    }

    public function test cannot remove shelf with games(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->once())->method('countGames')->with($this->isInstanceOf(GamesFilters::class))->willReturn(1);
        $this->repository->expects($this->never())->method('deleteShelf');

        $deleteShelf = new DeleteShelf($this->mainUser, $this->shelf->getUuid());

        $this->expectException(IncorrectActionException::class);
        $this->expectExceptionMessage('You cannot remove a shelf with games in it.');

        $this->handler->handle($deleteShelf);
    }

    public function test cannot remove not found shelf(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getShelf')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('deleteShelf');

        $deleteShelf = new DeleteShelf($this->mainUser, $uuid);

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($deleteShelf);
    }
}
