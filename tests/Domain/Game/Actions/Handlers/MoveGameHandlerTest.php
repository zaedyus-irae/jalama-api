<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\MoveGameHandler;
use ZI\Jalama\Domain\Game\Actions\MoveGame;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\GamePresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class MoveGameHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private MoveGameHandler $handler;
    private GamePresenterMock $gamePresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new MoveGameHandler($this->repository);
        $this->gamePresenterMock = new GamePresenterMock();
    }

    public function test can move a game(): void
    {
        $managedMainShelf = Shelf::new($this->mainUser, 'managed main shelf');
        $managedDestinationShelf = Shelf::new($this->mainUser, 'destination shelf');
        $ownedGame = Game::new($this->mainUser, $managedMainShelf, 'owned game');
        $this->repository->expects($this->once())->method('getGame')->with($ownedGame->getUuid())->willReturn($ownedGame);
        $this->repository->expects($this->once())->method('getShelf')->with($managedDestinationShelf->getUuid())->willReturn($managedDestinationShelf);
        $this->repository->expects($this->once())->method('saveGame')->with($ownedGame);

        $moveGame = new MoveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $ownedGame->getUuid(), $managedDestinationShelf->getUuid()
        );

        $this->assertSame($managedMainShelf, $ownedGame->getShelf());

        $this->handler->handle($moveGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($ownedGame->getUuid(), $game->getUuid());
        $this->assertSame($managedDestinationShelf, $game->getShelf());
    }

    public function test can move a owned game(): void
    {
        $notManagedMainShelf = Shelf::new($this->secondaryUser, 'not managed main shelf');
        $notManagedDestinationShelf = Shelf::new($this->secondaryUser, 'not managed destination shelf');
        $ownedGameFromNotManagedShelf = Game::new($this->mainUser, $notManagedMainShelf, 'owned game');
        $this->repository->expects($this->once())->method('getGame')->with($ownedGameFromNotManagedShelf->getUuid())->willReturn($ownedGameFromNotManagedShelf);
        $this->repository->expects($this->once())->method('getShelf')->with($notManagedDestinationShelf->getUuid())->willReturn($notManagedDestinationShelf);
        $this->repository->expects($this->once())->method('saveGame')->with($ownedGameFromNotManagedShelf);

        $moveGame = new MoveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $ownedGameFromNotManagedShelf->getUuid(),
            $notManagedDestinationShelf->getUuid()
        );

        $this->assertSame($notManagedMainShelf, $ownedGameFromNotManagedShelf->getShelf());

        $this->handler->handle($moveGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($ownedGameFromNotManagedShelf->getUuid(), $game->getUuid());
        $this->assertSame($notManagedDestinationShelf, $game->getShelf());
    }

    public function test can move a game from a managed shelf(): void
    {
        $managedMainShelf = Shelf::new($this->mainUser, 'managed main shelf');
        $notManagedDestinationShelf = Shelf::new($this->secondaryUser, 'not managed destination shelf');
        $notOwnedGameFromManagedShelf = Game::new($this->secondaryUser, $managedMainShelf, 'not owned game stored in managed shelf');
        $this->repository->expects($this->once())->method('getGame')->with($notOwnedGameFromManagedShelf->getUuid())->willReturn($notOwnedGameFromManagedShelf);
        $this->repository->expects($this->once())->method('getShelf')->with($notManagedDestinationShelf->getUuid())->willReturn($notManagedDestinationShelf);
        $this->repository->expects($this->once())->method('saveGame')->with($notOwnedGameFromManagedShelf);

        $moveGame = new MoveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $notOwnedGameFromManagedShelf->getUuid(),
            $notManagedDestinationShelf->getUuid()
        );

        $this->assertSame($managedMainShelf, $notOwnedGameFromManagedShelf->getShelf());

        $this->handler->handle($moveGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($notOwnedGameFromManagedShelf->getUuid(), $game->getUuid());
        $this->assertSame($notManagedDestinationShelf, $game->getShelf());
    }

    public function test can move a game to a managed shelf(): void
    {
        $notManagedMainShelf = Shelf::new($this->secondaryUser, 'not managed main shelf');
        $managedDestinationShelf = Shelf::new($this->mainUser, 'managed destination shelf');
        $notOwnedGameFromNotManagedShelf = Game::new($this->secondaryUser, $notManagedMainShelf, 'not owned game stored in a not managed shelf');
        $this->repository->expects($this->once())->method('getGame')->with($notOwnedGameFromNotManagedShelf->getUuid())->willReturn($notOwnedGameFromNotManagedShelf);
        $this->repository->expects($this->once())->method('getShelf')->with($managedDestinationShelf->getUuid())->willReturn($managedDestinationShelf);
        $this->repository->expects($this->once())->method('saveGame')->with($notOwnedGameFromNotManagedShelf);

        $moveGame = new MoveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $notOwnedGameFromNotManagedShelf->getUuid(),
            $managedDestinationShelf->getUuid()
        );

        $this->assertSame($notManagedMainShelf, $notOwnedGameFromNotManagedShelf->getShelf());

        $this->handler->handle($moveGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($notOwnedGameFromNotManagedShelf->getUuid(), $game->getUuid());
        $this->assertSame($managedDestinationShelf, $game->getShelf());
    }

    public function test cannot move a not owned game from or to a not managed shelf(): void
    {
        $notManagedMainShelf = Shelf::new($this->secondaryUser, 'not managed main shelf');
        $notManagedDestinationShelf = Shelf::new($this->secondaryUser, 'not managed destination shelf');
        $notOwnedGameFromNotManagedShelf = Game::new($this->secondaryUser, $notManagedMainShelf, 'not owned game stored in a not managed shelf');
        $this->repository->expects($this->once())->method('getGame')->with($notOwnedGameFromNotManagedShelf->getUuid())->willReturn($notOwnedGameFromNotManagedShelf);
        $this->repository->expects($this->once())->method('getShelf')->with($notManagedDestinationShelf->getUuid())->willReturn($notManagedDestinationShelf);
        $this->repository->expects($this->never())->method('saveGame');

        $moveGame = new MoveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $notOwnedGameFromNotManagedShelf->getUuid(),
            $notManagedDestinationShelf->getUuid()
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Cannot move a game you do not own, from and to a shelf you do not manage.');

        $this->handler->handle($moveGame);
    }

    public function test cannot move a not found game(): void
    {
        $uuid = Uuid::uuid4();
        $destinationShelf = Shelf::new($this->mainUser, 'destination shelf');
        $this->repository->expects($this->once())->method('getGame')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('getShelf');
        $this->repository->expects($this->never())->method('saveGame');

        $moveGame = new MoveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $uuid,
            $destinationShelf->getUuid()
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked game.');

        $this->handler->handle($moveGame);
    }

    public function test cannot move a game to a not found shelf(): void
    {
        $uuid = Uuid::uuid4();
        $mainShelf = Shelf::new($this->mainUser, 'main shelf');
        $game = Game::new($this->mainUser, $mainShelf, 'game');
        $this->repository->expects($this->once())->method('getGame')->with($game->getUuid())->willReturn($game);
        $this->repository->expects($this->once())->method('getShelf')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('saveGame');

        $moveGame = new MoveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $game->getUuid(),
            $uuid
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($moveGame);
    }
}
