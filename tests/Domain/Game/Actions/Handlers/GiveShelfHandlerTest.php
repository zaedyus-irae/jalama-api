<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\GiveShelf;
use ZI\Jalama\Domain\Game\Actions\Handlers\GiveShelfHandler;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\ShelfPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class GiveShelfHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $shelf;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private GiveShelfHandler $handler;
    private ShelfPresenterMock $shelfPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new GiveShelfHandler($this->repository);
        $this->shelfPresenterMock = new ShelfPresenterMock();
    }

    public function test can give a shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->once())->method('saveShelf')->with($this->shelf);

        $giveShelf = new GiveShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            $this->shelf->getUuid(),
            $this->secondaryUser
        );

        $this->assertSame($this->mainUser, $this->shelf->getOwner());

        $this->handler->handle($giveShelf);

        $shelf = $this->shelfPresenterMock->getResult();
        $this->assertInstanceOf(Shelf::class, $shelf);
        $this->assertSame($this->secondaryUser, $shelf->getOwner());
    }

    public function test cannot give a not owned shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('saveShelf');

        $giveShelf = new GiveShelf(
            $this->shelfPresenterMock,
            $this->secondaryUser,
            $this->shelf->getUuid(),
            $this->secondaryUser
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Only the owner of a shelf can give it to another user.');

        $this->handler->handle($giveShelf);
    }

    public function test cannot give a not found shelf(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getShelf')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('saveShelf');

        $giveShelf = new GiveShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            $uuid,
            $this->mainUser
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($giveShelf);
    }
}
