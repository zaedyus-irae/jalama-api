<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\GiveGame;
use ZI\Jalama\Domain\Game\Actions\Handlers\GiveGameHandler;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\GamePresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class GiveGameHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $shelf;
    private Game $game;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private GiveGameHandler $handler;
    private GamePresenterMock $gamePresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->game = Game::new($this->mainUser, $this->shelf, 'test game');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new GiveGameHandler($this->repository);
        $this->gamePresenterMock = new GamePresenterMock();
    }

    public function test can give a game(): void
    {
        $this->repository->expects($this->once())->method('getGame')->with($this->game->getUuid())->willReturn($this->game);
        $this->repository->expects($this->once())->method('saveGame')->with($this->game);

        $giveGame = new GiveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $this->game->getUuid(),
            $this->secondaryUser
        );

        $this->assertSame($this->mainUser, $this->game->getOwner());

        $this->handler->handle($giveGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($this->game->getUuid(), $game->getUuid());
        $this->assertSame($this->secondaryUser, $game->getOwner());
    }

    public function test cannot give a not owned game(): void
    {
        $this->repository->expects($this->once())->method('getGame')->with($this->game->getUuid())->willReturn($this->game);
        $this->repository->expects($this->never())->method('saveGame');

        $giveGame = new GiveGame(
            $this->gamePresenterMock,
            $this->secondaryUser,
            $this->game->getUuid(),
            $this->secondaryUser
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Only the owner of a game can give it to another user.');

        $this->handler->handle($giveGame);
    }

    public function test cannot give a not found game(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getGame')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('saveGame');

        $giveGame = new GiveGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $uuid,
            $this->mainUser
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked game.');

        $this->handler->handle($giveGame);
    }
}
