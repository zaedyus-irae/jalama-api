<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\CreateShelf;
use ZI\Jalama\Domain\Game\Actions\Handlers\CreateShelfHandler;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\ShelfPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class CreateShelfHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private CreateShelfHandler $handler;
    private ShelfPresenterMock $shelfPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new CreateShelfHandler($this->repository);
        $this->shelfPresenterMock = new ShelfPresenterMock();
    }

    public function test can create a shelf(): void
    {
        $this->repository->expects($this->once())->method('saveShelf')->withAnyParameters();

        $createShelf = new CreateShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            'test shelf',
            'test shelf description',
            'image:data'
        );

        $this->handler->handle($createShelf);

        $shelf = $this->shelfPresenterMock->getResult();
        $this->assertInstanceOf(Shelf::class, $shelf);
        $this->assertSame($this->mainUser, $shelf->getOwner());
        $this->assertSame('test shelf', $shelf->getName());
        $this->assertSame('test shelf description', $shelf->getDescription());
        $this->assertSame('image:data', $shelf->getCover());
        $this->assertCount(1, $shelf->getManagers());
        $this->assertSame($this->mainUser, $shelf->getManagers()[0]);
        $this->assertCount(1, $shelf->getUsers());
        $this->assertSame($this->mainUser, $shelf->getUsers()[0]);
    }
}
