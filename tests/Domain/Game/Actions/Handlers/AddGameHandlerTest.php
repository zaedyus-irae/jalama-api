<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\AddGame;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\AddGameHandler;
use ZI\Jalama\Domain\Game\Model\Author;
use ZI\Jalama\Domain\Game\Model\Authors;
use ZI\Jalama\Domain\Game\Model\DurationRequirement;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\PlayerRequirement;
use ZI\Jalama\Domain\Game\Model\Publisher;
use ZI\Jalama\Domain\Game\Model\Publishers;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\GamePresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class AddGameHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $shelf;
    private Game $baseGame;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private AddGameHandler $handler;
    private GamePresenterMock $gamePresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->shelf = Shelf::new($this->mainUser, 'shelf');
        $this->baseGame = Game::new($this->mainUser, $this->shelf, 'basegame');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new AddGameHandler($this->repository);
        $this->gamePresenterMock = new GamePresenterMock();
    }

    public function test can add a game(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->once())->method('getGame')->with($this->baseGame->getUuid())->willReturn($this->baseGame);
        $this->repository->expects($this->once())->method('saveGame')->withAnyParameters();

        $addShelf = new AddGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $this->shelf->getUuid(),
            'test game',
            'test game description',
            'image:data',
            new PlayerRequirement(1, 5),
            new DurationRequirement(1800, 3200),
            new Authors($author = Author::new('test author')),
            new Publishers($publisher = Publisher::new('test author')),
            2020,
            $this->baseGame->getUuid(),
            'bgg-reference'
        );

        $this->handler->handle($addShelf);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($this->mainUser, $game->getOwner());
        $this->assertSame($this->shelf, $game->getShelf());
        $this->assertSame('test game', $game->getName());
        $this->assertSame('test game description', $game->getDescription());
        $this->assertSame('image:data', $game->getCover());
        $this->assertSame(1, $game->getPlayerRequirement()->getMinNumber());
        $this->assertSame(5, $game->getPlayerRequirement()->getMaxNumber());
        $this->assertSame(1800, $game->getDurationRequirement()->getMinInSeconds());
        $this->assertSame(3200, $game->getDurationRequirement()->getMaxInSeconds());
        $this->assertCount(1, $game->getAuthors());
        $this->assertSame($author, $game->getAuthors()[0]);
        $this->assertCount(1, $game->getPublishers());
        $this->assertSame($publisher, $game->getPublishers()[0]);
        $this->assertSame(2020, $game->getYear());
        $this->assertSame($this->baseGame, $game->getBaseGame());
        $this->assertSame('bgg-reference', $game->getBoardGameGeekReference());
    }

    public function test cannot add a game in a not managed shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('getGame');
        $this->repository->expects($this->never())->method('saveGame');

        $addShelf = new AddGame(
            $this->gamePresenterMock,
            $this->secondaryUser,
            $this->shelf->getUuid(),
            'test game',
            'test game description',
            'image:data',
            new PlayerRequirement(1, 5),
            new DurationRequirement(1800, 3200),
            new Authors($author = Author::new('test author')),
            new Publishers($publisher = Publisher::new('test author')),
            2020,
            $this->baseGame->getUuid(),
            'bgg-reference'
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Cannot add a new game to a shelf you do not manage.');

        $this->handler->handle($addShelf);
    }

    public function test cannot add a game in a not found shelf(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getShelf')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('getGame');
        $this->repository->expects($this->never())->method('saveGame');

        $addShelf = new AddGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $uuid,
            'test game',
            'test game description',
            'image:data',
            new PlayerRequirement(1, 5),
            new DurationRequirement(1800, 3200),
            new Authors($author = Author::new('test author')),
            new Publishers($publisher = Publisher::new('test author')),
            2020,
            $this->baseGame->getUuid(),
            'bgg-reference'
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($addShelf);
    }

    public function test cannot add a game with a not found base game(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->once())->method('getGame')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('saveGame');

        $addShelf = new AddGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $this->shelf->getUuid(),
            'test game',
            'test game description',
            'image:data',
            new PlayerRequirement(1, 5),
            new DurationRequirement(1800, 3200),
            new Authors($author = Author::new('test author')),
            new Publishers($publisher = Publisher::new('test author')),
            2020,
            $uuid,
            'bgg-reference'
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked game.');

        $this->handler->handle($addShelf);
    }
}
