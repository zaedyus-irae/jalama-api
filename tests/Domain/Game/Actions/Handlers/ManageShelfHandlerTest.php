<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Account\Model\Users;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\IncorrectActionException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\ManageShelfHandler;
use ZI\Jalama\Domain\Game\Actions\ManageShelf;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\ShelfPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class ManageShelfHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private User $oldManagerUser;
    private User $oldUserUser;
    private User $newManagerUser;
    private User $newUserUser;
    private Shelf $shelf;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private ManageShelfHandler $handler;
    private ShelfPresenterMock $shelfPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->oldManagerUser = $this->generateUser('old manager');
        $this->oldUserUser = $this->generateUser('old user');
        $this->newManagerUser = $this->generateUser('new manager');
        $this->newUserUser = $this->generateUser('new user');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->shelf->setManagers(new Users($this->mainUser, $this->oldManagerUser));
        $this->shelf->setUsers(new Users($this->mainUser, $this->oldManagerUser, $this->oldUserUser));
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new ManageShelfHandler($this->repository);
        $this->shelfPresenterMock = new ShelfPresenterMock();
    }

    public function test can manage a shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->once())->method('saveShelf')->with($this->shelf);

        $manageShelf = new ManageShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            $this->shelf->getUuid(),
            new Users($this->newManagerUser),
            new Users($this->oldManagerUser),
            new Users($this->newUserUser),
            new Users($this->oldUserUser)
        );

        $this->assertCount(2, $this->shelf->getManagers());
        $this->assertSame($this->mainUser, $this->shelf->getManagers()[0]);
        $this->assertSame($this->oldManagerUser, $this->shelf->getManagers()[1]);
        $this->assertCount(3, $this->shelf->getUsers());
        $this->assertSame($this->mainUser, $this->shelf->getUsers()[0]);
        $this->assertSame($this->oldManagerUser, $this->shelf->getUsers()[1]);
        $this->assertSame($this->oldUserUser, $this->shelf->getUsers()[2]);

        $this->handler->handle($manageShelf);

        $shelf = $this->shelfPresenterMock->getResult();
        $this->assertInstanceOf(Shelf::class, $shelf);
        $this->assertSame($this->shelf->getUuid(), $shelf->getUuid());
        $this->assertCount(2, $shelf->getManagers());
        $this->assertSame($this->mainUser, $shelf->getManagers()[0]);
        $this->assertSame($this->newManagerUser, $shelf->getManagers()[1]);
        $this->assertCount(4, $shelf->getUsers());
        $this->assertSame($this->mainUser, $shelf->getUsers()[0]);
        $this->assertSame($this->oldManagerUser, $shelf->getUsers()[1]);
        $this->assertSame($this->newManagerUser, $shelf->getUsers()[2]);
        $this->assertSame($this->newUserUser, $shelf->getUsers()[3]);
    }

    public function test cannot manage a not managed shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('saveShelf');

        $manageShelf = new ManageShelf(
            $this->shelfPresenterMock,
            $this->secondaryUser,
            $this->shelf->getUuid(),
            null,
            null,
            null,
            null
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Only the manager of a shelf can manage it.');

        $this->handler->handle($manageShelf);
    }

    public function test cannot remove self from the managers of a shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('saveShelf');

        $manageShelf = new ManageShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            $this->shelf->getUuid(),
            null,
            new Users($this->mainUser),
            null,
            null
        );

        $this->expectException(IncorrectActionException::class);
        $this->expectExceptionMessage('You cannot remove yourself from the managers of a shelf.');

        $this->handler->handle($manageShelf);
    }

    public function test convert model exception to incorrect action exception owner manager(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('saveShelf');

        $manageShelf = new ManageShelf(
            $this->shelfPresenterMock,
            $this->oldManagerUser,
            $this->shelf->getUuid(),
            null,
            new Users($this->mainUser),
            null,
            null
        );

        $this->expectException(IncorrectActionException::class);
        $this->expectExceptionMessage('Cannot remove the owner from the managers.');

        $this->handler->handle($manageShelf);
    }

    public function test convert model exception to incorrect action exception owner user(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('saveShelf');

        $manageShelf = new ManageShelf(
            $this->shelfPresenterMock,
            $this->oldManagerUser,
            $this->shelf->getUuid(),
            null,
            null,
            null,
            new Users($this->mainUser)
        );

        $this->expectException(IncorrectActionException::class);
        $this->expectExceptionMessage('Cannot remove the owner from the users.');

        $this->handler->handle($manageShelf);
    }

    public function test convert model exception to incorrect action exception manager user(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('saveShelf');

        $manageShelf = new ManageShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            $this->shelf->getUuid(),
            null,
            null,
            null,
            new Users($this->oldManagerUser)
        );

        $this->expectException(IncorrectActionException::class);
        $this->expectExceptionMessage('Cannot remove a manager from the users.');

        $this->handler->handle($manageShelf);
    }

    public function test cannot manage a not found shelf(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getShelf')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('saveShelf');

        $manageShelf = new ManageShelf(
            $this->shelfPresenterMock,
            $this->secondaryUser,
            $uuid,
            null,
            null,
            null,
            null
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($manageShelf);
    }
}
