<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\ListShelfGamesHandler;
use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedGames;
use ZI\Jalama\Domain\Game\Actions\ListShelfGames;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\Games;
use ZI\Jalama\Domain\Game\Model\Listing\GamesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\GamesSorts;
use ZI\Jalama\Domain\Game\Model\Listing\GamesSortType;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\GamesPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class ListShelfGamesHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $shelf;
    private GamesSorts $sorts;
    private Pagination $pagination;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private ListShelfGamesHandler $handler;
    private GamesPresenterMock $gamesPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->sorts = new GamesSorts(GamesSortType::NAME());
        $this->pagination = new Pagination(1, 1, 5);
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new ListShelfGamesHandler($this->repository);
        $this->gamesPresenterMock = new GamesPresenterMock();
    }

    public function test can list games from shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $games = new Games(
            Game::new($this->mainUser, $this->shelf, 'game 1'),
            Game::new($this->mainUser, $this->shelf, 'game 2'),
            Game::new($this->mainUser, $this->shelf, 'game 3'),
            Game::new($this->mainUser, $this->shelf, 'game 4'),
            Game::new($this->mainUser, $this->shelf, 'game 5')
        );
        $this->repository->expects($this->once())->method('findGames')->with($this->isInstanceOf(GamesFilters::class), $this->sorts, $this->pagination)->willReturn($games);
        $this->repository->expects($this->once())->method('countGames')->with($this->isInstanceOf(GamesFilters::class))->willReturn(10);

        $listShelfGames = new ListShelfGames(
            $this->gamesPresenterMock,
            $this->mainUser,
            $this->shelf->getUuid(),
            $this->pagination,
            $this->sorts
        );

        $this->handler->handle($listShelfGames);

        $paginatedGames = $this->gamesPresenterMock->getResult();
        $this->assertInstanceOf(PaginatedGames::class, $paginatedGames);
        $this->assertSame($this->pagination, $paginatedGames->getPagination());
        $this->assertSame(10, $paginatedGames->getTotalNumberOfElements());
        $this->assertCount(5, $paginatedGames);
        $this->assertSame($games[0], $paginatedGames[0]);
        $this->assertSame($games[1], $paginatedGames[1]);
        $this->assertSame($games[2], $paginatedGames[2]);
        $this->assertSame($games[3], $paginatedGames[3]);
        $this->assertSame($games[4], $paginatedGames[4]);
    }

    public function test cannot list games from a not used shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('findGames');
        $this->repository->expects($this->never())->method('countGames');

        $listShelfGames = new ListShelfGames(
            $this->gamesPresenterMock,
            $this->secondaryUser,
            $this->shelf->getUuid(),
            $this->pagination,
            $this->sorts
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Cannot list games from a shelf you do not have access.');

        $this->handler->handle($listShelfGames);
    }

    public function test cannot list games from a not found shelf(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getShelf')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('findGames');
        $this->repository->expects($this->never())->method('countGames');

        $listShelfGames = new ListShelfGames(
            $this->gamesPresenterMock,
            $this->mainUser,
            $uuid,
            $this->pagination,
            $this->sorts
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($listShelfGames);
    }
}
