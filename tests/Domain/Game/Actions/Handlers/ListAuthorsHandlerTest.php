<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Handlers\ListAuthorsHandler;
use ZI\Jalama\Domain\Game\Actions\ListAuthors;
use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedAuthors;
use ZI\Jalama\Domain\Game\Model\Author;
use ZI\Jalama\Domain\Game\Model\Authors;
use ZI\Jalama\Domain\Game\Model\Listing\AuthorsFilters;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\AuthorsPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class ListAuthorsHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private Pagination $pagination;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private ListAuthorsHandler $handler;
    private AuthorsPresenterMock $authorsPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->pagination = new Pagination(1, 1, 5);
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new ListAuthorsHandler($this->repository);
        $this->authorsPresenterMock = new AuthorsPresenterMock();
    }

    public function test can list authors(): void
    {
        $authors = new Authors(
            Author::new('author 1'),
            Author::new('author 2'),
            Author::new('author 3'),
            Author::new('author 4'),
            Author::new('author 5')
        );
        $this->repository->expects($this->once())->method('findAuthors')->with($this->isInstanceOf(AuthorsFilters::class), $this->pagination)->willReturn($authors);
        $this->repository->expects($this->once())->method('countAuthors')->with($this->isInstanceOf(AuthorsFilters::class))->willReturn(10);

        $listAuthor = new ListAuthors(
            $this->authorsPresenterMock,
            $this->mainUser,
            $this->pagination
        );

        $this->handler->handle($listAuthor);

        $paginatedAuthors = $this->authorsPresenterMock->getResult();
        $this->assertInstanceOf(PaginatedAuthors::class, $paginatedAuthors);
        $this->assertSame($this->pagination, $paginatedAuthors->getPagination());
        $this->assertSame(10, $paginatedAuthors->getTotalNumberOfElements());
        $this->assertCount(5, $paginatedAuthors);
        $this->assertSame($authors[0], $paginatedAuthors[0]);
        $this->assertSame($authors[1], $paginatedAuthors[1]);
        $this->assertSame($authors[2], $paginatedAuthors[2]);
        $this->assertSame($authors[3], $paginatedAuthors[3]);
        $this->assertSame($authors[4], $paginatedAuthors[4]);
    }
}
