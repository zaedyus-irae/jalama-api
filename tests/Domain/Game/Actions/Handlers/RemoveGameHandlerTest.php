<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\RemoveGameHandler;
use ZI\Jalama\Domain\Game\Actions\RemoveGame;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class RemoveGameHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $shelf;
    private Game $game;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private RemoveGameHandler $handler;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->game = Game::new($this->mainUser, $this->shelf, 'test game');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new RemoveGameHandler($this->repository);
    }

    public function test can remove own game(): void
    {
        $this->repository->expects($this->once())->method('getGame')->with($this->game->getUuid())->willReturn($this->game);
        $this->repository->expects($this->once())->method('deleteGame')->with($this->game->getUuid());

        $removeGame = new RemoveGame($this->mainUser, $this->game->getUuid());

        $this->handler->handle($removeGame);
    }

    public function test cannot remove not owned game(): void
    {
        $this->repository->expects($this->once())->method('getGame')->with($this->game->getUuid())->willReturn($this->game);
        $this->repository->expects($this->never())->method('deleteGame');

        $removeGame = new RemoveGame($this->secondaryUser, $this->game->getUuid());

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Only the owner of a game can delete it.');

        $this->handler->handle($removeGame);
    }

    public function test cannot remove not found game(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getGame')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('deleteGame');

        $removeGame = new RemoveGame($this->mainUser, $uuid);

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked game.');

        $this->handler->handle($removeGame);
    }
}
