<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\ViewShelfHandler;
use ZI\Jalama\Domain\Game\Actions\ViewShelf;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\ShelfPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class ViewShelfHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $shelf;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private ViewShelfHandler $handler;
    private ShelfPresenterMock $shelfPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new ViewShelfHandler($this->repository);
        $this->shelfPresenterMock = new ShelfPresenterMock();
    }

    public function test can view a shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);

        $viewShelf = new ViewShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            $this->shelf->getUuid()
        );

        $this->handler->handle($viewShelf);

        $shelf = $this->shelfPresenterMock->getResult();
        $this->assertInstanceOf(Shelf::class, $shelf);
        $this->assertSame($this->shelf->getUuid(), $shelf->getUuid());
    }

    public function test cannot view a not used shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);

        $viewShelf = new ViewShelf(
            $this->shelfPresenterMock,
            $this->secondaryUser,
            $this->shelf->getUuid()
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Cannot view a shelf you do not have access.');

        $this->handler->handle($viewShelf);
    }

    public function test cannot view a not found shelf(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getShelf')->with($uuid)->willReturn(null);

        $viewShelf = new ViewShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            $uuid
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($viewShelf);
    }
}
