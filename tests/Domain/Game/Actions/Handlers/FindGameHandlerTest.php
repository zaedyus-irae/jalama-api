<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\EmptyOutcomeException;
use ZI\Jalama\Domain\Game\Actions\FindGame;
use ZI\Jalama\Domain\Game\Actions\Handlers\FindGameHandler;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\Games;
use ZI\Jalama\Domain\Game\Model\Listing\GamesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\GamesSorts;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\GamePresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class FindGameHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private Shelf $shelf;
    private Game $game;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private FindGameHandler $handler;
    private GamePresenterMock $gamePresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->game = Game::new($this->mainUser, $this->shelf, 'test shelf');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new FindGameHandler($this->repository);
        $this->gamePresenterMock = new GamePresenterMock();
    }

    public function test can find a random game(): void
    {
        $this->repository->expects($this->once())->method('findGames')->with($this->isInstanceOf(GamesFilters::class), $this->isInstanceOf(GamesSorts::class), $this->isInstanceOf(Pagination::class))->willReturn(new Games($this->game));

        $findGame = new FindGame(
            $this->gamePresenterMock,
            $this->mainUser
        );

        $this->handler->handle($findGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($this->game, $game);
    }

    public function test cannot find a random game(): void
    {
        $this->repository->expects($this->once())->method('findGames')->with($this->isInstanceOf(GamesFilters::class), $this->isInstanceOf(GamesSorts::class), $this->isInstanceOf(Pagination::class))->willReturn(new Games());

        $findGame = new FindGame(
            $this->gamePresenterMock,
            $this->mainUser
        );

        $this->expectException(EmptyOutcomeException::class);
        $this->expectExceptionMessage('Could not find a matching game.');

        $this->handler->handle($findGame);
    }

    public function test cannot find a game from not found shelf(): void
    {
        $shelf1 = Shelf::new($this->mainUser, 'shelf 1');
        $uuid = Uuid::uuid4();
        $shelf3 = Shelf::new($this->mainUser, 'shelf 3');
        $this->repository->expects($this->exactly(2))->method('getShelf')->withConsecutive([$shelf1->getUuid()], [$uuid])->willReturnOnConsecutiveCalls($shelf1, null);
        $this->repository->expects($this->never())->method('findGames');

        $findGame = new FindGame(
            $this->gamePresenterMock,
            $this->mainUser,
            [$shelf1->getUuid(), $uuid, $shelf3->getUuid()]
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($findGame);
    }
}
