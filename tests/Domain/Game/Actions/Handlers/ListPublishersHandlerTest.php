<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Handlers\ListPublishersHandler;
use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedPublishers;
use ZI\Jalama\Domain\Game\Actions\ListPublishers;
use ZI\Jalama\Domain\Game\Model\Listing\PublishersFilters;
use ZI\Jalama\Domain\Game\Model\Publisher;
use ZI\Jalama\Domain\Game\Model\Publishers;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\PublishersPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class ListPublishersHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private Pagination $pagination;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private ListPublishersHandler $handler;
    private PublishersPresenterMock $publishersPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->pagination = new Pagination(1, 1, 5);
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new ListPublishersHandler($this->repository);
        $this->publishersPresenterMock = new PublishersPresenterMock();
    }

    public function test can list publishers(): void
    {
        $publishers = new Publishers(
            Publisher::new('publisher 1'),
            Publisher::new('publisher 2'),
            Publisher::new('publisher 3'),
            Publisher::new('publisher 4'),
            Publisher::new('publisher 5')
        );
        $this->repository->expects($this->once())->method('findPublishers')->with($this->isInstanceOf(PublishersFilters::class), $this->pagination)->willReturn($publishers);
        $this->repository->expects($this->once())->method('countPublishers')->with($this->isInstanceOf(PublishersFilters::class))->willReturn(10);

        $listPublisher = new ListPublishers(
            $this->publishersPresenterMock,
            $this->mainUser,
            $this->pagination
        );

        $this->handler->handle($listPublisher);

        $paginatedPublishers = $this->publishersPresenterMock->getResult();
        $this->assertInstanceOf(PaginatedPublishers::class, $paginatedPublishers);
        $this->assertSame($this->pagination, $paginatedPublishers->getPagination());
        $this->assertSame(10, $paginatedPublishers->getTotalNumberOfElements());
        $this->assertCount(5, $paginatedPublishers);
        $this->assertSame($publishers[0], $paginatedPublishers[0]);
        $this->assertSame($publishers[1], $paginatedPublishers[1]);
        $this->assertSame($publishers[2], $paginatedPublishers[2]);
        $this->assertSame($publishers[3], $paginatedPublishers[3]);
        $this->assertSame($publishers[4], $paginatedPublishers[4]);
    }
}
