<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\EditShelf;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\EditShelfHandler;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\ShelfPresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class EditShelfHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $shelf;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private EditShelfHandler $handler;
    private ShelfPresenterMock $shelfPresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new EditShelfHandler($this->repository);
        $this->shelfPresenterMock = new ShelfPresenterMock();
    }

    public function test can edit a shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->once())->method('saveShelf')->with($this->shelf);

        $editShelf = new EditShelf(
            $this->shelfPresenterMock,
            $this->mainUser,
            $this->shelf->getUuid(),
            'edited shelf',
            'edited shelf description',
            'image:data'
        );

        $this->assertSame($this->mainUser, $this->shelf->getOwner());
        $this->assertSame('test shelf', $this->shelf->getName());
        $this->assertSame('', $this->shelf->getDescription());
        $this->assertSame(null, $this->shelf->getCover());

        $this->handler->handle($editShelf);

        $shelf = $this->shelfPresenterMock->getResult();
        $this->assertInstanceOf(Shelf::class, $shelf);
        $this->assertSame($this->shelf->getUuid(), $shelf->getUuid());
        $this->assertSame($this->mainUser, $shelf->getOwner());
        $this->assertSame('edited shelf', $shelf->getName());
        $this->assertSame('edited shelf description', $shelf->getDescription());
        $this->assertSame('image:data', $shelf->getCover());
    }

    public function test cannot edit a not managed shelf(): void
    {
        $this->repository->expects($this->once())->method('getShelf')->with($this->shelf->getUuid())->willReturn($this->shelf);
        $this->repository->expects($this->never())->method('saveShelf');

        $editShelf = new EditShelf(
            $this->shelfPresenterMock,
            $this->secondaryUser,
            $this->shelf->getUuid(),
            'edited shelf',
            'edited shelf description',
            'image:data'
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Only the manager of a shelf can edit it.');

        $this->handler->handle($editShelf);
    }

    public function test cannot edit a not found shelf(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getShelf')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('saveShelf');

        $editShelf = new EditShelf(
            $this->shelfPresenterMock,
            $this->secondaryUser,
            $uuid,
            'edited shelf',
            'edited shelf description',
            'image:data'
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked shelf.');

        $this->handler->handle($editShelf);
    }
}
