<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\EditGame;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Handlers\EditGameHandler;
use ZI\Jalama\Domain\Game\Model\Author;
use ZI\Jalama\Domain\Game\Model\Authors;
use ZI\Jalama\Domain\Game\Model\DurationRequirement;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\PlayerRequirement;
use ZI\Jalama\Domain\Game\Model\Publisher;
use ZI\Jalama\Domain\Game\Model\Publishers;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\Game\Actions\Outcomes\GamePresenterMock;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class EditGameHandlerTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $shelf;
    private Game $game;
    private Game $baseGame;
    /** @var \PHPUnit\Framework\MockObject\MockObject|RepositoryInterface */
    private RepositoryInterface $repository;
    private EditGameHandler $handler;
    private GamePresenterMock $gamePresenterMock;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('visitor');
        $this->shelf = Shelf::new($this->mainUser, 'test shelf');
        $this->game = Game::new($this->mainUser, $this->shelf, 'test game');
        $this->baseGame = Game::new($this->mainUser, $this->shelf, 'base game');
        $this->repository = $this->createMock(RepositoryInterface::class);
        $this->handler = new EditGameHandler($this->repository);
        $this->gamePresenterMock = new GamePresenterMock();
    }

    public function test can edit a shelf(): void
    {
        $this->repository->expects($this->exactly(2))->method('getGame')->withConsecutive([$this->game->getUuid()], [$this->baseGame->getUuid()])->willReturnOnConsecutiveCalls($this->game, $this->baseGame);
        $this->repository->expects($this->once())->method('saveGame')->with($this->game);

        $editGame = new EditGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $this->game->getUuid(),
            'edited game',
            'edited game description',
            'image:data',
            new PlayerRequirement(1, 5),
            new DurationRequirement(1800, 3200),
            new Authors($author = Author::new('test author')),
            new Publishers($publisher = Publisher::new('test publisher')),
            2020,
            $this->baseGame->getUuid(),
            'bgg-reference'
        );

        $this->assertSame($this->mainUser, $this->game->getOwner());
        $this->assertSame($this->shelf, $this->game->getShelf());
        $this->assertSame('test game', $this->game->getName());
        $this->assertSame('', $this->game->getDescription());
        $this->assertSame(null, $this->game->getCover());
        $this->assertSame(null, $this->game->getPlayerRequirement()->getMinNumber());
        $this->assertSame(null, $this->game->getPlayerRequirement()->getMaxNumber());
        $this->assertSame(null, $this->game->getDurationRequirement()->getMinInSeconds());
        $this->assertSame(null, $this->game->getDurationRequirement()->getMaxInSeconds());
        $this->assertCount(0, $this->game->getAuthors());
        $this->assertCount(0, $this->game->getPublishers());
        $this->assertSame(null, $this->game->getYear());
        $this->assertSame(null, $this->game->getBaseGame());
        $this->assertSame(null, $this->game->getBoardGameGeekReference());

        $this->handler->handle($editGame);

        $game = $this->gamePresenterMock->getResult();
        $this->assertInstanceOf(Game::class, $game);
        $this->assertSame($this->game->getUuid(), $game->getUuid());
        $this->assertSame($this->mainUser, $game->getOwner());
        $this->assertSame($this->shelf, $game->getShelf());
        $this->assertSame('edited game', $game->getName());
        $this->assertSame('edited game description', $game->getDescription());
        $this->assertSame('image:data', $game->getCover());
        $this->assertSame(1, $game->getPlayerRequirement()->getMinNumber());
        $this->assertSame(5, $game->getPlayerRequirement()->getMaxNumber());
        $this->assertSame(1800, $game->getDurationRequirement()->getMinInSeconds());
        $this->assertSame(3200, $game->getDurationRequirement()->getMaxInSeconds());
        $this->assertCount(1, $game->getAuthors());
        $this->assertSame($author, $game->getAuthors()[0]);
        $this->assertCount(1, $game->getPublishers());
        $this->assertSame($publisher, $game->getPublishers()[0]);
        $this->assertSame(2020, $game->getYear());
        $this->assertSame($this->baseGame, $game->getBaseGame());
        $this->assertSame('bgg-reference', $game->getBoardGameGeekReference());
    }

    public function test cannot edit a not owned game(): void
    {
        $this->repository->expects($this->once())->method('getGame')->with($this->game->getUuid())->willReturn($this->game);
        $this->repository->expects($this->never())->method('saveGame');

        $editGame = new EditGame(
            $this->gamePresenterMock,
            $this->secondaryUser,
            $this->game->getUuid(),
            'edited game',
            'edited game description',
            'image:data',
            new PlayerRequirement(1, 5),
            new DurationRequirement(1800, 3200),
            new Authors(Author::new('test author')),
            new Publishers(Publisher::new('test publisher')),
            2020,
            $this->baseGame->getUuid(),
            'bgg-reference'
        );

        $this->expectException(UnauthorizedActionException::class);
        $this->expectExceptionMessage('Cannot edit a game you do not own.');

        $this->handler->handle($editGame);
    }

    public function test cannot edit a not found game(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->once())->method('getGame')->with($uuid)->willReturn(null);
        $this->repository->expects($this->never())->method('saveGame');

        $editGame = new EditGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $uuid,
            'edited game',
            'edited game description',
            'image:data',
            new PlayerRequirement(1, 5),
            new DurationRequirement(1800, 3200),
            new Authors(Author::new('test author')),
            new Publishers(Publisher::new('test publisher')),
            2020,
            null,
            'bgg-reference'
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked game.');

        $this->handler->handle($editGame);
    }

    public function test cannot edit a not found base game(): void
    {
        $uuid = Uuid::uuid4();
        $this->repository->expects($this->exactly(2))->method('getGame')->withConsecutive([$this->game->getUuid()], [$uuid])->willReturnOnConsecutiveCalls($this->game, null);
        $this->repository->expects($this->never())->method('saveGame');

        $editGame = new EditGame(
            $this->gamePresenterMock,
            $this->mainUser,
            $this->game->getUuid(),
            'edited game',
            'edited game description',
            'image:data',
            new PlayerRequirement(1, 5),
            new DurationRequirement(1800, 3200),
            new Authors(Author::new('test author')),
            new Publishers(Publisher::new('test publisher')),
            2020,
            $uuid,
            'bgg-reference'
        );

        $this->expectException(ActionTargetNotFoundException::class);
        $this->expectExceptionMessage('Cannot find the asked game.');

        $this->handler->handle($editGame);
    }
}
