<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Outcomes\GamePresenter;
use ZI\JalamaTests\Domain\Shared\Actions\Outcomes\ResultsHolderMockTrait;

class GamePresenterMock implements GamePresenter
{
    use ResultsHolderMockTrait;
}
