<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Outcomes\PublishersPresenter;
use ZI\JalamaTests\Domain\Shared\Actions\Outcomes\ResultsHolderMockTrait;

class PublishersPresenterMock implements PublishersPresenter
{
    use ResultsHolderMockTrait;
}
