<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Outcomes\AuthorsPresenter;
use ZI\JalamaTests\Domain\Shared\Actions\Outcomes\ResultsHolderMockTrait;

class AuthorsPresenterMock implements AuthorsPresenter
{
    use ResultsHolderMockTrait;
}
