<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Outcomes\ShelfPresenter;
use ZI\JalamaTests\Domain\Shared\Actions\Outcomes\ResultsHolderMockTrait;

class ShelfPresenterMock implements ShelfPresenter
{
    use ResultsHolderMockTrait;
}
