<?php

namespace ZI\JalamaTests\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Outcomes\GamesPresenter;
use ZI\JalamaTests\Domain\Shared\Actions\Outcomes\ResultsHolderMockTrait;

class GamesPresenterMock implements GamesPresenter
{
    use ResultsHolderMockTrait;
}
