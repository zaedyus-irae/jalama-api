<?php

namespace ZI\JalamaTests\Domain\Game\Model;

use ZI\Jalama\Domain\Game\Model\Publisher;
use ZI\JalamaTests\TestCase;

class PublisherTest extends TestCase
{
    public function test publisher initialization(): Publisher
    {
        $publisher = Publisher::new('test publisher');

        $this->assertSame('test publisher', $publisher->getName());
        $this->assertNotEmpty($publisher->getUuid()->toString());

        return $publisher;
    }
}
