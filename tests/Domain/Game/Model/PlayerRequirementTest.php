<?php

namespace ZI\JalamaTests\Domain\Game\Model;

use ZI\Jalama\Domain\Game\Model\PlayerRequirement;
use ZI\JalamaTests\TestCase;

class PlayerRequirementTest extends TestCase
{
    /**
     * @dataProvider getAcceptedValues
     */
    public function test accepted value for player requirement(?int $minPlayer, ?int $maxPlayer): void
    {
        $playerRequirement = new PlayerRequirement($minPlayer, $maxPlayer);

        $this->assertSame($minPlayer, $playerRequirement->getMinNumber());
        $this->assertSame($maxPlayer, $playerRequirement->getMaxNumber());
    }

    /**
     * @return \Generator<string,array>
     */
    public function getAcceptedValues(): \Generator
    {
        yield 'unknown' => [null, null];
        yield 'max' => [null, 7200];
        yield 'min-max' => [null, 1];
        yield 'min' => [3600, null];
        yield 'min-min' => [1, null];
        yield 'range' => [3600, 7200];
        yield 'fixed' => [5400, 5400];
    }

    /**
     * @dataProvider getRejectedValues
     */
    public function test invalid value for player requirement throws an exception(?int $minPlayer, ?int $maxPlayer, string $expectedMessage): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($expectedMessage);

        $playerRequirement = new PlayerRequirement($minPlayer, $maxPlayer);
    }

    /**
     * @return \Generator<string,array>
     */
    public function getRejectedValues(): \Generator
    {
        yield 'negative-min' => [-3600, null, 'Minimum player should be a positive integer, or null.'];
        yield 'zero-min' => [0, null, 'Minimum player should be a positive integer, or null.'];
        yield 'negative-max' => [null, -7200, 'Maximum player should be a positive integer, or null.'];
        yield 'zero-max' => [null, 0, 'Maximum player should be a positive integer, or null.'];
        yield 'both-negative-equals' => [-5400, -5200, 'Minimum player should be a positive integer, or null.'];
        yield 'min-superior-max' => [7200, 3600, 'Maximum player should not be lower than minimum player.'];
    }
}
