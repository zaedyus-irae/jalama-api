<?php

namespace ZI\JalamaTests\Domain\Game\Model;

use ZI\Jalama\Domain\Game\Model\Author;
use ZI\JalamaTests\TestCase;

class AuthorTest extends TestCase
{
    public function test author initialization(): Author
    {
        $author = Author::new('test author');

        $this->assertSame('test author', $author->getName());
        $this->assertNotEmpty($author->getUuid()->toString());

        return $author;
    }
}
