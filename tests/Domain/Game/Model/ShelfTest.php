<?php

namespace ZI\JalamaTests\Domain\Game\Model;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Account\Model\Users;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class ShelfTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('spectator');
    }

    public function test shelf initialization(): void
    {
        $shelf = Shelf::new($this->mainUser, 'test shelf');

        $this->assertSame($this->mainUser, $shelf->getOwner());
        $this->assertSame('test shelf', $shelf->getName());
        $this->assertSame('', $shelf->getDescription());
        $this->assertNull($shelf->getCover());
        $this->assertCount(1, $shelf->getManagers());
        $this->assertTrue($shelf->getManagers()->contains($this->mainUser));
        $this->assertCount(1, $shelf->getUsers());
        $this->assertTrue($shelf->getUsers()->contains($this->mainUser));
    }

    /**
     * @depends test shelf initialization
     */
    public function test changing the owner adds it to the managers and users(): void
    {
        $shelf = Shelf::new($this->mainUser, 'test shelf');
        $shelf->setOwner($this->secondaryUser);

        $this->assertSame($this->secondaryUser, $shelf->getOwner());
        $this->assertCount(2, $shelf->getManagers());
        $this->assertTrue($shelf->getManagers()->contains($this->mainUser));
        $this->assertTrue($shelf->getManagers()->contains($this->secondaryUser));
        $this->assertCount(2, $shelf->getUsers());
        $this->assertTrue($shelf->getUsers()->contains($this->mainUser));
        $this->assertTrue($shelf->getUsers()->contains($this->secondaryUser));
    }

    /**
     * @depends test shelf initialization
     */
    public function test cannot remove the owner from the managers(): void
    {
        $shelf = Shelf::new($this->mainUser, 'test shelf');

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Cannot remove the owner from the managers.');

        $shelf->setManagers(new Users($this->secondaryUser));
    }

    /**
     * @depends test shelf initialization
     */
    public function test cannot remove the owner from the users(): void
    {
        $shelf = Shelf::new($this->mainUser, 'test shelf');

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Cannot remove the owner from the users.');

        $shelf->setUsers(new Users($this->secondaryUser));
    }

    /**
     * @depends test shelf initialization
     */
    public function test adding a manager also adds it to the users(): void
    {
        $shelf = Shelf::new($this->mainUser, 'test shelf');

        $shelf->setManagers(new Users($this->mainUser, $this->secondaryUser));

        $this->assertCount(2, $shelf->getManagers());
        $this->assertTrue($shelf->getManagers()->contains($this->mainUser));
        $this->assertTrue($shelf->getManagers()->contains($this->secondaryUser));
        $this->assertCount(2, $shelf->getUsers());
        $this->assertTrue($shelf->getUsers()->contains($this->mainUser));
        $this->assertTrue($shelf->getUsers()->contains($this->secondaryUser));
    }

    /**
     * @depends test adding a manager also adds it to the users
     */
    public function test cannot remove a manager from the users(): void
    {
        $shelf = Shelf::new($this->mainUser, 'test shelf');
        $shelf->setManagers(new Users($this->mainUser, $this->secondaryUser));

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Cannot remove a manager from the users.');

        $shelf->setUsers(new Users($this->mainUser));
    }

    /**
     * @depends test shelf initialization
     */
    public function test can add a user(): void
    {
        $shelf = Shelf::new($this->mainUser, 'test shelf');

        $shelf->setUsers(new Users($this->mainUser, $this->secondaryUser));

        $this->assertCount(2, $shelf->getUsers());
        $this->assertTrue($shelf->getUsers()->contains($this->mainUser));
        $this->assertTrue($shelf->getUsers()->contains($this->secondaryUser));
    }
}
