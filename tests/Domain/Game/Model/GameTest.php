<?php

namespace ZI\JalamaTests\Domain\Game\Model;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\JalamaTests\Domain\UserGeneratorTrait;
use ZI\JalamaTests\TestCase;

class GameTest extends TestCase
{
    use UserGeneratorTrait;

    private User $mainUser;
    private User $secondaryUser;
    private Shelf $mainShelf1;
    private Shelf $mainShelf2;
    private Shelf $secondaryShelf1;

    public function setUp(): void
    {
        $this->mainUser = $this->generateUser('tester');
        $this->secondaryUser = $this->generateUser('spectator');

        $this->mainShelf1 = Shelf::new($this->mainUser, '1');
        $this->mainShelf2 = Shelf::new($this->mainUser, '2');
        $this->secondaryShelf1 = Shelf::new($this->secondaryUser, '1');
    }

    public function test game initialization(): void
    {
        $game = Game::new($this->mainUser, $this->mainShelf1, 'test game');

        $this->assertSame($this->mainUser, $game->getOwner());
        $this->assertSame($this->mainShelf1, $game->getShelf());
        $this->assertNull($game->getBoardGameGeekReference());
        $this->assertSame('test game', $game->getName());
        $this->assertSame('', $game->getDescription());
        $this->assertNull($game->getCover());
        $this->assertNull($game->getPlayerRequirement()->getMinNumber());
        $this->assertNull($game->getPlayerRequirement()->getMaxNumber());
        $this->assertNull($game->getDurationRequirement()->getMinInSeconds());
        $this->assertNull($game->getDurationRequirement()->getMaxInSeconds());
        $this->assertEmpty($game->getAuthors());
        $this->assertEmpty($game->getPublishers());
        $this->assertNull($game->getYear());
        $this->assertNull($game->getBaseGame());
    }

    /**
     * @dataProvider getValidYearsForAGame
     * @depends test game initialization
     */
    public function test setting year with correct values(int $year): void
    {
        $game = Game::new($this->mainUser, $this->mainShelf1, 'test game');

        $game->setYear($year);

        $this->assertSame($year, $game->getYear());
    }

    /**
     * @return \Generator<array>
     */
    public function getValidYearsForAGame(): \Generator
    {
        yield '1800' => [1800];

        yield '2020' => [2020];

        yield '2999' => [2999];

        yield 'rand' => [rand(1800, 2999)];
    }

    /**
     * @dataProvider getInvValidYearsForAGame
     * @depends test game initialization
     */
    public function test setting year with incorrect values(int $year): void
    {
        $game = Game::new($this->mainUser, $this->mainShelf1, 'test game');

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('This model only handle game between 1800 and 3000');

        $game->setYear($year);
    }

    /**
     * @return \Generator<array>
     */
    public function getInvValidYearsForAGame(): \Generator
    {
        yield '1799' => [1799];

        yield '3000' => [3000];

        yield 'before rand' => [rand(0, 1799)];

        yield 'after rand' => [rand(3000, 999999)];

        yield 'negative value' => [-rand(1800, 2999)];
    }
}
