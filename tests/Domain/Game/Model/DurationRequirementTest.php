<?php

namespace ZI\JalamaTests\Domain\Game\Model;

use ZI\Jalama\Domain\Game\Model\DurationRequirement;
use ZI\JalamaTests\TestCase;

class DurationRequirementTest extends TestCase
{
    /**
     * @dataProvider getAcceptedValues
     */
    public function test accepted value for duration requirement(?int $minDuration, ?int $maxDuration): void
    {
        $durationRequirement = new DurationRequirement($minDuration, $maxDuration);

        $this->assertSame($minDuration, $durationRequirement->getMinInSeconds());
        $this->assertSame($maxDuration, $durationRequirement->getMaxInSeconds());
    }

    /**
     * @return \Generator<string,array>
     */
    public function getAcceptedValues(): \Generator
    {
        yield 'unknown' => [null, null];
        yield 'max' => [null, 7200];
        yield 'min-max' => [null, 1];
        yield 'min' => [3600, null];
        yield 'min-min' => [1, null];
        yield 'range' => [3600, 7200];
        yield 'fixed' => [5400, 5400];
    }

    /**
     * @dataProvider getRejectedValues
     */
    public function test invalid value for duration requirement throws an exception(?int $minDuration, ?int $maxDuration, string $expectedMessage): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($expectedMessage);

        $durationRequirement = new DurationRequirement($minDuration, $maxDuration);
    }

    /**
     * @return \Generator<string,array>
     */
    public function getRejectedValues(): \Generator
    {
        yield 'negative-min' => [-3600, null, 'Minimum duration should be a positive integer, or null.'];
        yield 'zero-min' => [0, null, 'Minimum duration should be a positive integer, or null.'];
        yield 'negative-max' => [null, -7200, 'Maximum duration should be a positive integer, or null.'];
        yield 'zero-max' => [null, 0, 'Maximum duration should be a positive integer, or null.'];
        yield 'both-negative-equals' => [-5400, -5200, 'Minimum duration should be a positive integer, or null.'];
        yield 'min-superior-max' => [7200, 3600, 'Maximum duration should not be lower than minimum duration.'];
    }
}
