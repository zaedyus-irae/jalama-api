<?php

namespace ZI\JalamaTests\Domain\Game\Model\Listing;

use ZI\Jalama\Domain\Game\Model\Listing\YearFilter;
use ZI\JalamaTests\TestCase;

class YearFilterTest extends TestCase
{
    /**
     * @dataProvider getAcceptedValues
     */
    public function test accepted value for duration requirement(?int $after, ?int $before): void
    {
        $durationRequirement = new YearFilter($after, $before);

        $this->assertSame($after, $durationRequirement->getAfter());
        $this->assertSame($before, $durationRequirement->getBefore());
    }

    /**
     * @return \Generator<string,array>
     */
    public function getAcceptedValues(): \Generator
    {
        yield 'unknown' => [null, null];
        yield 'max' => [null, 2020];
        yield 'min' => [2000, null];
        yield 'range' => [2000, 2020];
        yield 'fixed' => [2010, 2010];
    }

    /**
     * @dataProvider getRejectedValues
     */
    public function test invalid value for duration requirement throws an exception(?int $after, ?int $before, string $expectedMessage): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($expectedMessage);

        $durationRequirement = new YearFilter($after, $before);
    }

    /**
     * @return \Generator<string,array>
     */
    public function getRejectedValues(): \Generator
    {
        yield 'min-superior-max' => [2020, 2000, 'To search a period, before must be greater than after'];
    }
}
