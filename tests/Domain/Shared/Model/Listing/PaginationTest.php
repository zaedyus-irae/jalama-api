<?php

namespace ZI\JalamaTests\Domain\Shared\Model\Listing;

use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;
use ZI\JalamaTests\TestCase;

class PaginationTest extends TestCase
{
    /**
     * @dataProvider getFirstElementCalculationTestCases
     */
    public function test get first element(int $startPage, int $resultsPerPages, int $expectedFirstElement): void
    {
        $pagination = new Pagination($startPage, 1, $resultsPerPages);

        $this->assertSame($expectedFirstElement, $pagination->getFirstElement());
    }

    /**
     * @return \Generator<string,array>
     */
    public function getFirstElementCalculationTestCases(): \Generator
    {
        yield 'first page with 1 element per page' => [1, 1, 0];

        yield 'second page with 1 element per page' => [2, 1, 1];

        yield 'tenth page with 1 element per page' => [10, 1, 9];

        yield 'first page with 7 element per page' => [1, 7, 0];

        yield 'second page with 7 element per page' => [2, 7, 7];

        yield 'tenth page with 7 element per page' => [10, 7, 63];
    }

    /**
     * @dataProvider getRejectedValues
     */
    public function test invalid value for pagination throws an exception(int $startPage, int $numberOfPages, int $resultsPerPages, string $expectedMessage): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($expectedMessage);

        $pagination = new Pagination($startPage, $numberOfPages, $resultsPerPages);
    }

    /**
     * @return \Generator<string,array>
     */
    public function getRejectedValues(): \Generator
    {
        yield 'negative-start-page' => [-1, 1, 1, 'Cannot ask for page before the first.'];
        yield 'zero-start-page' => [0, 1, 1, 'Cannot ask for page before the first.'];
        yield 'negative-number-of-pages' => [1, -1, 1, 'Cannot ask for a null or negative number of pages.'];
        yield 'zero-number-of-pages' => [1, 0, 1, 'Cannot ask for a null or negative number of pages.'];
        yield 'negative-results-per-pages' => [1, 1, -1, 'Cannot ask for a null or negative number of results per page.'];
        yield 'zero-results-per-pages' => [1, 1, 0, 'Cannot ask for a null or negative number of results per page.'];
    }
}
