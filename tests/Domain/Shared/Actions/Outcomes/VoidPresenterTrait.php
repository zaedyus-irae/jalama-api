<?php

namespace ZI\JalamaTests\Domain\Shared\Actions\Outcomes;

trait VoidPresenterTrait
{
    /**
     * @param mixed $result
     */
    public function present($result): void
    {
        // Do nothing.
    }
}
