<?php

namespace ZI\JalamaTests\Domain\Shared\Actions\Outcomes;

trait ResultsHolderMockTrait
{
    /** @var mixed */
    private $result;

    /**
     * @param mixed $result
     */
    public function present($result): void
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }
}
