<?php

namespace ZI\JalamaTests\Domain;

use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Model\User;

trait UserGeneratorTrait
{
    private function generateUser(string $displayName): User
    {
        return User::new(Uuid::uuid5(Uuid::uuid5(Uuid::NIL, self::class), $displayName), $displayName);
    }
}
