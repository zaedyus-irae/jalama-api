<?php

namespace ZI\Jalama\Domain\Shared;

use MyCLabs\Enum\Enum;

trait UniquelyEnumSetTraits
{
    public function contains($element, bool $strict = true): bool
    {
        return in_array(
            $element->getValue(),
            array_map(
                function (Enum $element): string {
                    return $element->getValue();
                },
                $this->data
            ),
            $strict
        );
    }
}
