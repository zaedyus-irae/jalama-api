<?php

namespace ZI\Jalama\Domain\Shared;

trait UniquelyIdentifiedSetTraits
{
    /**
     * @param UniquelyIdentifiedInterface $element
     */
    public function contains($element, bool $strict = true): bool
    {
        return in_array(
            $element->getUuid()->toString(),
            array_map(
                function (UniquelyIdentifiedInterface $element): string {
                    return $element->getUuid()->toString();
                },
                $this->data
            ),
            $strict
        );
    }
}
