<?php

namespace ZI\Jalama\Domain\Shared\Actions\Listing;

use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;

trait PaginatedSetTrait
{
    private Pagination $pagination;
    private int $totalNumberOfElements;

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getTotalNumberOfElements(): int
    {
        return $this->totalNumberOfElements;
    }
}
