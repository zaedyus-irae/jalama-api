<?php

namespace ZI\Jalama\Domain\Shared\Actions;

use ZI\Jalama\Domain\Shared\Actions\Outcomes\ResultsHolder;

interface ReadingActionInterface
{
    public function getResultsHolder(): ResultsHolder;
}
