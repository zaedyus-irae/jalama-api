<?php

namespace ZI\Jalama\Domain\Shared;

use Ramsey\Uuid\UuidInterface;

interface UniquelyIdentifiedInterface
{
    public function getUuid(): UuidInterface;
}
