<?php

namespace ZI\Jalama\Domain\Shared;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

trait UniquelyIdentifiedTrait
{
    private UuidInterface $uuid;

    private static function getRandomUuid(): UuidInterface
    {
        return Uuid::uuid4();
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
