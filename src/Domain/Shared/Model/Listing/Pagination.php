<?php

namespace ZI\Jalama\Domain\Shared\Model\Listing;

final class Pagination
{
    private int $startPage;
    private int $numberOfPages;
    private int $resultsPerPages;

    public function __construct(
        int $startPage,
        int $numberOfPages,
        int $resultsPerPages
    ) {
        if ($startPage < 1) {
            throw new \InvalidArgumentException('Cannot ask for page before the first.');
        }
        if ($numberOfPages < 1) {
            throw new \InvalidArgumentException('Cannot ask for a null or negative number of pages.');
        }
        if ($resultsPerPages < 1) {
            throw new \InvalidArgumentException('Cannot ask for a null or negative number of results per page.');
        }

        $this->startPage = $startPage;
        $this->numberOfPages = $numberOfPages;
        $this->resultsPerPages = $resultsPerPages;
    }

    public function getStartPage(): int
    {
        return $this->startPage;
    }

    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }

    public function getResultsPerPages(): int
    {
        return $this->resultsPerPages;
    }

    public function getFirstElement(): int
    {
        return ($this->startPage - 1) * $this->resultsPerPages;
    }
}
