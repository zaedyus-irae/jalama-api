<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\GamePresenter;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;

final class ViewGame implements ReadingActionInterface, UserActionInterface, GameActionInterface
{
    private GamePresenter $gamePresenter;
    private User $user;
    private UuidInterface $gameUuid;

    public function __construct(
        GamePresenter $gamePresenter,
        User $user,
        UuidInterface $gameUuid
    ) {
        $this->gamePresenter = $gamePresenter;
        $this->user = $user;
        $this->gameUuid = $gameUuid;
    }

    public function getResultsHolder(): GamePresenter
    {
        return $this->gamePresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getGameUuid(): UuidInterface
    {
        return $this->gameUuid;
    }
}
