<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;

interface GameActionInterface
{
    public function getGameUuid(): UuidInterface;
}
