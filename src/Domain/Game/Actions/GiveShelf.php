<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\ShelfPresenter;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class GiveShelf implements WritingActionInterface, UserActionInterface, ShelfActionInterface, RecipientActionInterface
{
    private ShelfPresenter $shelfPresenter;
    private User $user;
    private UuidInterface $gameUuid;
    private User $recipient;

    public function __construct(
        ShelfPresenter $shelfPresenter,
        User $user,
        UuidInterface $gameUuid,
        User $recipient
    ) {
        $this->shelfPresenter = $shelfPresenter;
        $this->user = $user;
        $this->gameUuid = $gameUuid;
        $this->recipient = $recipient;
    }

    public function getResultsHolder(): ShelfPresenter
    {
        return $this->shelfPresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getShelfUuid(): UuidInterface
    {
        return $this->gameUuid;
    }

    public function getRecipient(): User
    {
        return $this->recipient;
    }
}
