<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;

interface ShelfActionInterface
{
    public function getShelfUuid(): UuidInterface;
}
