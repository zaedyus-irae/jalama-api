<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\GamePresenter;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class GiveGame implements WritingActionInterface, UserActionInterface, GameActionInterface, RecipientActionInterface
{
    private GamePresenter $gamePresenter;
    private User $user;
    private UuidInterface $gameUuid;
    private User $recipient;

    public function __construct(
        GamePresenter $gamePresenter,
        User $user,
        UuidInterface $gameUuid,
        User $recipient
    ) {
        $this->gamePresenter = $gamePresenter;
        $this->user = $user;
        $this->gameUuid = $gameUuid;
        $this->recipient = $recipient;
    }

    public function getResultsHolder(): GamePresenter
    {
        return $this->gamePresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getGameUuid(): UuidInterface
    {
        return $this->gameUuid;
    }

    public function getRecipient(): User
    {
        return $this->recipient;
    }
}
