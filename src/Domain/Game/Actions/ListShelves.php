<?php

namespace ZI\Jalama\Domain\Game\Actions;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\ShelvesPresenter;
use ZI\Jalama\Domain\Game\Model\Listing\ShelvesSorts;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;

final class ListShelves implements ReadingActionInterface, UserActionInterface
{
    private ShelvesPresenter $shelvesPresenter;
    private User $user;
    private Pagination $pagination;
    private ShelvesSorts $shelvesSorts;
    private ?string $text;

    public function __construct(
        ShelvesPresenter $shelvesPresenter,
        User $user,
        Pagination $pagination,
        ShelvesSorts $shelvesSorts,
        ?string $text = null
    ) {
        $this->shelvesPresenter = $shelvesPresenter;
        $this->user = $user;
        $this->pagination = $pagination;
        $this->shelvesSorts = $shelvesSorts;
        $this->text = $text;
    }

    public function getResultsHolder(): ShelvesPresenter
    {
        return $this->shelvesPresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getShelvesSorts(): ShelvesSorts
    {
        return $this->shelvesSorts;
    }

    public function getText(): ?string
    {
        return $this->text;
    }
}
