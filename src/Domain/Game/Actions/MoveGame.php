<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\GamePresenter;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class MoveGame implements WritingActionInterface, UserActionInterface, GameActionInterface, ShelfActionInterface
{
    private GamePresenter $gamePresenter;
    private User $user;
    private UuidInterface $gameUuid;
    private UuidInterface $shelfUuid;

    public function __construct(
        GamePresenter $gamePresenter,
        User $user,
        UuidInterface $gameUuid,
        UuidInterface $shelfUuid
    ) {
        $this->gamePresenter = $gamePresenter;
        $this->user = $user;
        $this->gameUuid = $gameUuid;
        $this->shelfUuid = $shelfUuid;
    }

    public function getResultsHolder(): GamePresenter
    {
        return $this->gamePresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getGameUuid(): UuidInterface
    {
        return $this->gameUuid;
    }

    public function getShelfUuid(): UuidInterface
    {
        return $this->shelfUuid;
    }
}
