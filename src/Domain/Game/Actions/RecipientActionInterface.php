<?php

namespace ZI\Jalama\Domain\Game\Actions;

use ZI\Jalama\Domain\Account\Model\User;

interface RecipientActionInterface
{
    public function getRecipient(): User;
}
