<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class RemoveGame implements WritingActionInterface, UserActionInterface, GameActionInterface
{
    private User $user;
    private UuidInterface $gameUuid;

    public function __construct(
        User $user,
        UuidInterface $gameUuid
    ) {
        $this->user = $user;
        $this->gameUuid = $gameUuid;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getGameUuid(): UuidInterface
    {
        return $this->gameUuid;
    }
}
