<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class DeleteShelf implements WritingActionInterface, UserActionInterface, ShelfActionInterface
{
    private User $user;
    private UuidInterface $shelfUuid;

    public function __construct(
        User $user,
        UuidInterface $shelfUuid
    ) {
        $this->user = $user;
        $this->shelfUuid = $shelfUuid;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getShelfUuid(): UuidInterface
    {
        return $this->shelfUuid;
    }
}
