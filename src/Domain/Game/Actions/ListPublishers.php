<?php

namespace ZI\Jalama\Domain\Game\Actions;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\PublishersPresenter;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;

final class ListPublishers implements ReadingActionInterface, UserActionInterface
{
    private PublishersPresenter $publishersPresenter;
    private User $user;
    private Pagination $pagination;
    private ?string $text;

    public function __construct(
        PublishersPresenter $publishersPresenter,
        User $user,
        Pagination $pagination,
        ?string $text = null
    ) {
        $this->publishersPresenter = $publishersPresenter;
        $this->user = $user;
        $this->pagination = $pagination;
        $this->text = $text;
    }

    public function getResultsHolder(): PublishersPresenter
    {
        return $this->publishersPresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getText(): ?string
    {
        return $this->text;
    }
}
