<?php

namespace ZI\Jalama\Domain\Game\Actions;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\ShelfPresenter;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class CreateShelf implements WritingActionInterface, UserActionInterface, ReadingActionInterface
{
    private ShelfPresenter $shelfPresenter;
    private User $user;
    private string $name;
    private string $description;
    private ?string $cover;

    public function __construct(
        ShelfPresenter $shelfPresenter,
        User $user,
        string $name,
        string $description,
        ?string $cover
    ) {
        $this->shelfPresenter = $shelfPresenter;
        $this->user = $user;
        $this->name = $name;
        $this->description = $description;
        $this->cover = $cover;
    }

    public function getResultsHolder(): ShelfPresenter
    {
        return $this->shelfPresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }
}
