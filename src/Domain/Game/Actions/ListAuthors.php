<?php

namespace ZI\Jalama\Domain\Game\Actions;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\AuthorsPresenter;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;

final class ListAuthors implements ReadingActionInterface, UserActionInterface
{
    private AuthorsPresenter $authorsPresenter;
    private User $user;
    private Pagination $pagination;
    private ?string $text;

    public function __construct(
        AuthorsPresenter $authorsPresenter,
        User $user,
        Pagination $pagination,
        ?string $text = null
    ) {
        $this->authorsPresenter = $authorsPresenter;
        $this->user = $user;
        $this->pagination = $pagination;
        $this->text = $text;
    }

    public function getResultsHolder(): AuthorsPresenter
    {
        return $this->authorsPresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getText(): ?string
    {
        return $this->text;
    }
}
