<?php

namespace ZI\Jalama\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\Jalama\Domain\Shared\Actions\Outcomes\ResultsHolder;

interface ShelfPresenter extends ResultsHolder
{
    public function present(Shelf $result): void;
}
