<?php

namespace ZI\Jalama\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedShelves;
use ZI\Jalama\Domain\Shared\Actions\Outcomes\ResultsHolder;

interface ShelvesPresenter extends ResultsHolder
{
    public function present(PaginatedShelves $result): void;
}
