<?php

namespace ZI\Jalama\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedPublishers;
use ZI\Jalama\Domain\Shared\Actions\Outcomes\ResultsHolder;

interface PublishersPresenter extends ResultsHolder
{
    public function present(PaginatedPublishers $result): void;
}
