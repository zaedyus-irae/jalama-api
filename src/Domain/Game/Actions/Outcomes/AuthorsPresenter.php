<?php

namespace ZI\Jalama\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedAuthors;
use ZI\Jalama\Domain\Shared\Actions\Outcomes\ResultsHolder;

interface AuthorsPresenter extends ResultsHolder
{
    public function present(PaginatedAuthors $result): void;
}
