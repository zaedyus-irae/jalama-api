<?php

namespace ZI\Jalama\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedGames;
use ZI\Jalama\Domain\Shared\Actions\Outcomes\ResultsHolder;

interface GamesPresenter extends ResultsHolder
{
    public function present(PaginatedGames $result): void;
}
