<?php

namespace ZI\Jalama\Domain\Game\Actions\Outcomes;

use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Shared\Actions\Outcomes\ResultsHolder;

interface GamePresenter extends ResultsHolder
{
    public function present(Game $result): void;
}
