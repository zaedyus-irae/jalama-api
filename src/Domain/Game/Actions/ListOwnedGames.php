<?php

namespace ZI\Jalama\Domain\Game\Actions;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\GamesPresenter;
use ZI\Jalama\Domain\Game\Model\Authors;
use ZI\Jalama\Domain\Game\Model\DurationRequirement;
use ZI\Jalama\Domain\Game\Model\Listing\GamesSorts;
use ZI\Jalama\Domain\Game\Model\PlayerRequirement;
use ZI\Jalama\Domain\Game\Model\Publishers;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;

final class ListOwnedGames implements ReadingActionInterface, UserActionInterface
{
    private GamesPresenter $gamesPresenter;
    private User $user;
    private Pagination $pagination;
    private GamesSorts $gamesSorts;
    private ?string $text;
    private ?PlayerRequirement $playerRequirement;
    private ?DurationRequirement $durationRequirement;
    private ?Authors $authors;
    private ?Publishers $publishers;
    private ?int $minYear;
    private ?int $maxYear;

    public function __construct(
        GamesPresenter $gamesPresenter,
        User $user,
        Pagination $pagination,
        GamesSorts $gamesSorts,
        ?string $text = null,
        ?PlayerRequirement $playerRequirement = null,
        ?DurationRequirement $durationRequirement = null,
        ?Authors $authors = null,
        ?Publishers $publishers = null,
        ?int $minYear = null,
        ?int $maxYear = null
    ) {
        $this->gamesPresenter = $gamesPresenter;
        $this->user = $user;
        $this->pagination = $pagination;
        $this->gamesSorts = $gamesSorts;
        $this->text = $text;
        $this->playerRequirement = $playerRequirement;
        $this->durationRequirement = $durationRequirement;
        $this->authors = $authors;
        $this->publishers = $publishers;
        $this->minYear = $minYear;
        $this->maxYear = $maxYear;
    }

    public function getResultsHolder(): GamesPresenter
    {
        return $this->gamesPresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getGamesSorts(): GamesSorts
    {
        return $this->gamesSorts;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function getPlayerRequirement(): ?PlayerRequirement
    {
        return $this->playerRequirement;
    }

    public function getDurationRequirement(): ?DurationRequirement
    {
        return $this->durationRequirement;
    }

    public function getAuthors(): ?Authors
    {
        return $this->authors;
    }

    public function getPublishers(): ?Publishers
    {
        return $this->publishers;
    }

    public function getMinYear(): ?int
    {
        return $this->minYear;
    }

    public function getMaxYear(): ?int
    {
        return $this->maxYear;
    }
}
