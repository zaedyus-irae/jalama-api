<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\EditGame;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;

final class EditGameHandler extends AbstractHandler
{
    public function handle(EditGame $editGame): void
    {
        $game = $this->getGame($editGame->getGameUuid());

        if (!$game->getOwner()->getUuid()->equals($editGame->getUser()->getUuid())) {
            throw new UnauthorizedActionException('Cannot edit a game you do not own.');
        }

        $game->setName($editGame->getName());
        $game->setDescription($editGame->getDescription());
        $game->setCover($editGame->getCover());
        $game->setPlayerRequirement($editGame->getPlayerRequirement());
        $game->setDurationRequirement($editGame->getDurationRequirement());
        $game->setAuthors($editGame->getAuthors());
        $game->setPublishers($editGame->getPublishers());
        $game->setYear($editGame->getYear());
        $game->setBaseGame($editGame->getBaseGameUuid() !== null ? $this->getGame($editGame->getBaseGameUuid()) : null);
        $game->setBoardGameGeekReference($editGame->getBoardGameGeekReference());

        $this->repository->saveGame($game);

        $editGame->getResultsHolder()->present($game);
    }
}
