<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Account\Model\Users;
use ZI\Jalama\Domain\Game\Actions\Exceptions\IncorrectActionException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\ManageShelf;

final class ManageShelfHandler extends AbstractHandler
{
    public function handle(ManageShelf $manageShelf): void
    {
        $shelf = $this->getShelf($manageShelf->getShelfUuid());
        if (!$shelf->getManagers()->contains($manageShelf->getUser())) {
            throw new UnauthorizedActionException('Only the manager of a shelf can manage it.');
        }

        if ($manageShelf->getOldManagers() !== null && $manageShelf->getOldManagers()->contains($manageShelf->getUser())) {
            throw new IncorrectActionException('You cannot remove yourself from the managers of a shelf.');
        }

        $managers = $shelf->getManagers();
        foreach ($manageShelf->getNewManagers() ?? [] as $newManager) {
            $managers->add($newManager);
        }
        foreach ($manageShelf->getOldManagers() ?? [] as $oldManager) {
            $managers->remove($oldManager);
        }
        try {
            $shelf->setManagers(new Users(...$managers->toArray()));
        } catch (\InvalidArgumentException $e) {
            throw new IncorrectActionException($e->getMessage(), 0, $e);
        }

        $users = $shelf->getUsers();
        foreach ($manageShelf->getNewUsers() ?? [] as $newUser) {
            $users->add($newUser);
        }
        foreach ($manageShelf->getOldUsers() ?? [] as $oldUser) {
            $users->remove($oldUser);
        }
        try {
            $shelf->setUsers(new Users(...$users->toArray()));
        } catch (\InvalidArgumentException $e) {
            throw new IncorrectActionException($e->getMessage(), 0, $e);
        }

        $this->repository->saveShelf($shelf);

        $manageShelf->getResultsHolder()->present($shelf);
    }
}
