<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\DeleteShelf;
use ZI\Jalama\Domain\Game\Actions\Exceptions\IncorrectActionException;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Model\Listing\GamesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilter;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilterType;
use ZI\Jalama\Domain\Game\Model\Shelves;

final class DeleteShelfHandler extends AbstractHandler
{
    public function handle(DeleteShelf $deleteShelf): void
    {
        $shelf = $this->getShelf($deleteShelf->getShelfUuid());
        if (!$shelf->getOwner()->getUuid()->equals($deleteShelf->getUser()->getUuid())) {
            throw new UnauthorizedActionException('Only the owner of a shelf can delete it.');
        }
        $shelfGameFilter = new GamesFilters(
            new UserFilter($shelf->getOwner(), UserFilterType::ALL_VIEWABLE()),
            new Shelves($shelf),
            null,
            null,
            null,
            null,
            null,
            null
        );
        if ($this->repository->countGames($shelfGameFilter) > 0) {
            throw new IncorrectActionException('You cannot remove a shelf with games in it.');
        }

        $this->repository->deleteShelf($deleteShelf->getShelfUuid());
    }
}
