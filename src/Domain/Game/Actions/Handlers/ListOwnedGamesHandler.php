<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedGames;
use ZI\Jalama\Domain\Game\Actions\ListOwnedGames;
use ZI\Jalama\Domain\Game\Model\Listing\GamesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilter;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilterType;
use ZI\Jalama\Domain\Game\Model\Listing\YearFilter;

final class ListOwnedGamesHandler extends AbstractHandler
{
    public function handle(ListOwnedGames $listOwnedGames): void
    {
        $filter = new GamesFilters(
            new UserFilter($listOwnedGames->getUser(), UserFilterType::OWNER_ONLY()),
            null,
            $listOwnedGames->getText(),
            $listOwnedGames->getPlayerRequirement(),
            $listOwnedGames->getDurationRequirement(),
            $listOwnedGames->getAuthors(),
            $listOwnedGames->getPublishers(),
            new YearFilter($listOwnedGames->getMinYear(), $listOwnedGames->getMaxYear())
        );

        $listOwnedGames->getResultsHolder()->present(
            new PaginatedGames(
                $listOwnedGames->getPagination(),
                $this->repository->countGames($filter),
                ...$this->repository->findGames($filter, $listOwnedGames->getGamesSorts(), $listOwnedGames->getPagination())
            )
        );
    }
}
