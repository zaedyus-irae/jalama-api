<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\CreateShelf;
use ZI\Jalama\Domain\Game\Model\Shelf;

final class CreateShelfHandler extends AbstractHandler
{
    public function handle(CreateShelf $createShelf): void
    {
        $shelf = Shelf::new($createShelf->getUser(), $createShelf->getName());

        $shelf->setDescription($createShelf->getDescription());
        $shelf->setCover($createShelf->getCover());

        $this->repository->saveShelf($shelf);

        $createShelf->getResultsHolder()->present($shelf);
    }
}
