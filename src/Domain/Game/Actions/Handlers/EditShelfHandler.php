<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\EditShelf;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;

final class EditShelfHandler extends AbstractHandler
{
    public function handle(EditShelf $editShelf): void
    {
        $shelf = $this->getShelf($editShelf->getShelfUuid());
        if (!$shelf->getManagers()->contains($editShelf->getUser())) {
            throw new UnauthorizedActionException('Only the manager of a shelf can edit it.');
        }

        $shelf->setName($editShelf->getName());
        $shelf->setDescription($editShelf->getDescription());
        $shelf->setCover($editShelf->getCover());

        $this->repository->saveShelf($shelf);

        $editShelf->getResultsHolder()->present($shelf);
    }
}
