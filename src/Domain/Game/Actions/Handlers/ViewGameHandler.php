<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\ViewGame;

final class ViewGameHandler extends AbstractHandler
{
    public function handle(ViewGame $viewGame): void
    {
        $game = $this->getGame($viewGame->getGameUuid());
        if (!$game->getOwner()->getUuid()->equals($viewGame->getUser()->getUuid()) && !$game->getShelf()->getUsers()->contains($viewGame->getUser())) {
            throw new UnauthorizedActionException('Cannot view a game in a shelf you do not have access.');
        }

        $viewGame->getResultsHolder()->present($game);
    }
}
