<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedShelves;
use ZI\Jalama\Domain\Game\Actions\ListShelves;
use ZI\Jalama\Domain\Game\Model\Listing\ShelvesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilter;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilterType;

final class ListShelvesHandler extends AbstractHandler
{
    public function handle(ListShelves $listShelves): void
    {
        $filter = new ShelvesFilters(
            new UserFilter($listShelves->getUser(), UserFilterType::ALL_VIEWABLE()),
            $listShelves->getText()
        );

        $listShelves->getResultsHolder()->present(
            new PaginatedShelves(
                $listShelves->getPagination(),
                $this->repository->countShelves($filter),
                ...$this->repository->findShelves($filter, $listShelves->getShelvesSorts(), $listShelves->getPagination())
            )
        );
    }
}
