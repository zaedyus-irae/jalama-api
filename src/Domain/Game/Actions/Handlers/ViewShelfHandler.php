<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\ViewShelf;

final class ViewShelfHandler extends AbstractHandler
{
    public function handle(ViewShelf $viewShelf): void
    {
        $shelf = $this->getShelf($viewShelf->getShelfUuid());
        if (!$shelf->getUsers()->contains($viewShelf->getUser())) {
            throw new UnauthorizedActionException('Cannot view a shelf you do not have access.');
        }

        $viewShelf->getResultsHolder()->present($shelf);
    }
}
