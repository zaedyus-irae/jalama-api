<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\RemoveGame;

final class RemoveGameHandler extends AbstractHandler
{
    public function handle(RemoveGame $removeGame): void
    {
        $game = $this->getGame($removeGame->getGameUuid());
        if (!$game->getOwner()->getUuid()->equals($removeGame->getUser()->getUuid())) {
            throw new UnauthorizedActionException('Only the owner of a game can delete it.');
        }

        $this->repository->deleteGame($removeGame->getGameUuid());
    }
}
