<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedPublishers;
use ZI\Jalama\Domain\Game\Actions\ListPublishers;
use ZI\Jalama\Domain\Game\Model\Listing\PublishersFilters;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilter;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilterType;

final class ListPublishersHandler extends AbstractHandler
{
    public function handle(ListPublishers $listPublishers): void
    {
        $filter = new PublishersFilters(
            new UserFilter($listPublishers->getUser(), UserFilterType::ALL_VIEWABLE()),
            $listPublishers->getText()
        );

        $listPublishers->getResultsHolder()->present(
            new PaginatedPublishers(
                $listPublishers->getPagination(),
                $this->repository->countPublishers($filter),
                ...$this->repository->findPublishers($filter, $listPublishers->getPagination())
            )
        );
    }
}
