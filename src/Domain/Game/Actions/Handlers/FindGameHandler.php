<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Exceptions\EmptyOutcomeException;
use ZI\Jalama\Domain\Game\Actions\FindGame;
use ZI\Jalama\Domain\Game\Model\Listing\GamesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\GamesSorts;
use ZI\Jalama\Domain\Game\Model\Listing\GamesSortType;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilter;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilterType;
use ZI\Jalama\Domain\Game\Model\Listing\YearFilter;
use ZI\Jalama\Domain\Game\Model\Shelves;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;

final class FindGameHandler extends AbstractHandler
{
    public function handle(FindGame $findGame): void
    {
        if ($shelvesUuids = $findGame->getShelvesUuids()) {
            $shelves = new Shelves();
            foreach ($findGame->getShelvesUuids() ?? [] as $shelfUuid) {
                $shelves->add($this->getShelf($shelfUuid));
            }
        }

        $filter = new GamesFilters(
            new UserFilter($findGame->getUser(), UserFilterType::ALL_VIEWABLE()),
            $shelves ?? null,
            $findGame->getText(),
            $findGame->getPlayerRequirement(),
            $findGame->getDurationRequirement(),
            $findGame->getAuthors(),
            $findGame->getPublishers(),
            new YearFilter($findGame->getMinYear(), $findGame->getMaxYear())
        );
        $sorts = new GamesSorts(GamesSortType::RANDOM());
        $pagination = new Pagination(1, 1, 1);

        $games = $this->repository->findGames($filter, $sorts, $pagination);
        if (count($games) === 0) {
            throw new EmptyOutcomeException('Could not find a matching game.');
        }

        $findGame->getResultsHolder()->present($games->first());
    }
}
