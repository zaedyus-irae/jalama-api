<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\MoveGame;

final class MoveGameHandler extends AbstractHandler
{
    public function handle(MoveGame $moveGame): void
    {
        $game = $this->getGame($moveGame->getGameUuid());
        $shelf = $this->getShelf($moveGame->getShelfUuid());
        if (!$game->getOwner()->getUuid()->equals($moveGame->getUser()->getUuid())
            && !$game->getShelf()->getManagers()->contains($moveGame->getUser())
            && !$shelf->getManagers()->contains($moveGame->getUser())
        ) {
            throw new UnauthorizedActionException('Cannot move a game you do not own, from and to a shelf you do not manage.');
        }

        $game->setShelf($shelf);

        $this->repository->saveGame($game);

        $moveGame->getResultsHolder()->present($game);
    }
}
