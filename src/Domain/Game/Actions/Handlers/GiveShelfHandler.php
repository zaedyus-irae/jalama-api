<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\GiveShelf;

final class GiveShelfHandler extends AbstractHandler
{
    public function handle(GiveShelf $giveShelf): void
    {
        $shelf = $this->getShelf($giveShelf->getShelfUuid());
        if (!$shelf->getOwner()->getUuid()->equals($giveShelf->getUser()->getUuid())) {
            throw new UnauthorizedActionException('Only the owner of a shelf can give it to another user.');
        }

        $shelf->setOwner($giveShelf->getRecipient());

        $this->repository->saveShelf($shelf);

        $giveShelf->getResultsHolder()->present($shelf);
    }
}
