<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedGames;
use ZI\Jalama\Domain\Game\Actions\ListShelfGames;
use ZI\Jalama\Domain\Game\Model\Listing\GamesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilter;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilterType;
use ZI\Jalama\Domain\Game\Model\Listing\YearFilter;
use ZI\Jalama\Domain\Game\Model\Shelves;

final class ListShelfGamesHandler extends AbstractHandler
{
    public function handle(ListShelfGames $listShelfGames): void
    {
        $shelf = $this->getShelf($listShelfGames->getShelfUuid());
        if (!$shelf->getUsers()->contains($listShelfGames->getUser())) {
            throw new UnauthorizedActionException('Cannot list games from a shelf you do not have access.');
        }
        $filter = new GamesFilters(
            new UserFilter($listShelfGames->getUser(), UserFilterType::ALL_VIEWABLE()),
            new Shelves($shelf),
            $listShelfGames->getText(),
            $listShelfGames->getPlayerRequirement(),
            $listShelfGames->getDurationRequirement(),
            $listShelfGames->getAuthors(),
            $listShelfGames->getPublishers(),
            new YearFilter($listShelfGames->getMinYear(), $listShelfGames->getMaxYear())
        );

        $listShelfGames->getResultsHolder()->present(
            new PaginatedGames(
                $listShelfGames->getPagination(),
                $this->repository->countGames($filter),
                ...$this->repository->findGames($filter, $listShelfGames->getGamesSorts(), $listShelfGames->getPagination())
            )
        );
    }
}
