<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\AddGame;
use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Model\Game;

final class AddGameHandler extends AbstractHandler
{
    public function handle(AddGame $addGame): void
    {
        $shelf = $this->getShelf($addGame->getShelfUuid());
        if (!$shelf->getManagers()->contains($addGame->getUser())) {
            throw new UnauthorizedActionException('Cannot add a new game to a shelf you do not manage.');
        }

        $game = Game::new($addGame->getUser(), $shelf, $addGame->getName());
        $game->setDescription($addGame->getDescription());
        $game->setCover($addGame->getCover());
        $game->setPlayerRequirement($addGame->getPlayerRequirement());
        $game->setDurationRequirement($addGame->getDurationRequirement());
        $game->setAuthors($addGame->getAuthors());
        $game->setPublishers($addGame->getPublishers());
        $game->setYear($addGame->getYear());
        $game->setBaseGame($addGame->getBaseGameUuid() !== null ? $this->getGame($addGame->getBaseGameUuid()) : null);
        $game->setBoardGameGeekReference($addGame->getBoardGameGeekReference());

        $this->repository->saveGame($game);

        $addGame->getResultsHolder()->present($game);
    }
}
