<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Game\Actions\Exceptions\ActionTargetNotFoundException;
use ZI\Jalama\Domain\Game\Model\Game;
use ZI\Jalama\Domain\Game\Model\RepositoryInterface;
use ZI\Jalama\Domain\Game\Model\Shelf;

abstract class AbstractHandler
{
    protected RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    protected function getShelf(UuidInterface $shelfUuid): Shelf
    {
        $shelf = $this->repository->getShelf($shelfUuid);

        if ($shelf === null) {
            throw new ActionTargetNotFoundException('Cannot find the asked shelf.');
        }

        return $shelf;
    }

    protected function getGame(UuidInterface $gameUuid): Game
    {
        $game = $this->repository->getGame($gameUuid);

        if ($game === null) {
            throw new ActionTargetNotFoundException('Cannot find the asked game.');
        }

        return $game;
    }
}
