<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\Exceptions\UnauthorizedActionException;
use ZI\Jalama\Domain\Game\Actions\GiveGame;

final class GiveGameHandler extends AbstractHandler
{
    public function handle(GiveGame $giveGame): void
    {
        $game = $this->getGame($giveGame->getGameUuid());
        if (!$game->getOwner()->getUuid()->equals($giveGame->getUser()->getUuid())) {
            throw new UnauthorizedActionException('Only the owner of a game can give it to another user.');
        }

        $game->setOwner($giveGame->getRecipient());

        $this->repository->saveGame($game);

        $giveGame->getResultsHolder()->present($game);
    }
}
