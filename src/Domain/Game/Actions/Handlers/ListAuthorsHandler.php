<?php

namespace ZI\Jalama\Domain\Game\Actions\Handlers;

use ZI\Jalama\Domain\Game\Actions\ListAuthors;
use ZI\Jalama\Domain\Game\Actions\Listing\PaginatedAuthors;
use ZI\Jalama\Domain\Game\Model\Listing\AuthorsFilters;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilter;
use ZI\Jalama\Domain\Game\Model\Listing\UserFilterType;

final class ListAuthorsHandler extends AbstractHandler
{
    public function handle(ListAuthors $listAuthors): void
    {
        $filter = new AuthorsFilters(
            new UserFilter($listAuthors->getUser(), UserFilterType::ALL_VIEWABLE()),
            $listAuthors->getText()
        );

        $listAuthors->getResultsHolder()->present(
            new PaginatedAuthors(
                $listAuthors->getPagination(),
                $this->repository->countAuthors($filter),
                ...$this->repository->findAuthors($filter, $listAuthors->getPagination())
            )
        );
    }
}
