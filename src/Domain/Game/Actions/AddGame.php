<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\GamePresenter;
use ZI\Jalama\Domain\Game\Model\Authors;
use ZI\Jalama\Domain\Game\Model\DurationRequirement;
use ZI\Jalama\Domain\Game\Model\PlayerRequirement;
use ZI\Jalama\Domain\Game\Model\Publishers;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class AddGame implements WritingActionInterface, UserActionInterface, ShelfActionInterface, ReadingActionInterface
{
    private GamePresenter $gamePresenter;
    private User $user;
    private UuidInterface $shelfUuid;
    private string $name;
    private string $description;
    private ?string $cover;
    private PlayerRequirement $playerRequirement;
    private DurationRequirement $durationRequirement;
    private Authors $authors;
    private Publishers $publishers;
    private ?int $year;
    private ?UuidInterface $baseGameUuid;
    private ?string $boardGameGeekReference;

    public function __construct(
        GamePresenter $gamePresenter,
        User $user,
        UuidInterface $shelfUuid,
        string $name,
        string $description,
        ?string $cover,
        PlayerRequirement $playerRequirement,
        DurationRequirement $durationRequirement,
        Authors $authors,
        Publishers $publishers,
        ?int $year,
        ?UuidInterface $baseGameUuid,
        ?string $boardGameGeekReference
    ) {
        $this->gamePresenter = $gamePresenter;
        $this->user = $user;
        $this->shelfUuid = $shelfUuid;
        $this->name = $name;
        $this->description = $description;
        $this->cover = $cover;
        $this->playerRequirement = $playerRequirement;
        $this->durationRequirement = $durationRequirement;
        $this->authors = $authors;
        $this->publishers = $publishers;
        $this->year = $year;
        $this->baseGameUuid = $baseGameUuid;
        $this->boardGameGeekReference = $boardGameGeekReference;
    }

    public function getResultsHolder(): GamePresenter
    {
        return $this->gamePresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getShelfUuid(): UuidInterface
    {
        return $this->shelfUuid;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function getPlayerRequirement(): PlayerRequirement
    {
        return $this->playerRequirement;
    }

    public function getDurationRequirement(): DurationRequirement
    {
        return $this->durationRequirement;
    }

    public function getAuthors(): Authors
    {
        return $this->authors;
    }

    public function getPublishers(): Publishers
    {
        return $this->publishers;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function getBaseGameUuid(): ?UuidInterface
    {
        return $this->baseGameUuid;
    }

    public function getBoardGameGeekReference(): ?string
    {
        return $this->boardGameGeekReference;
    }
}
