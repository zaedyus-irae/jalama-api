<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Account\Model\Users;
use ZI\Jalama\Domain\Game\Actions\Outcomes\ShelfPresenter;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class ManageShelf implements WritingActionInterface, UserActionInterface, ShelfActionInterface
{
    private ShelfPresenter $shelfPresenter;
    private User $user;
    private UuidInterface $shelfUuid;
    private ?Users $newManagers;
    private ?Users $oldManagers;
    private ?Users $newUsers;
    private ?Users $oldUsers;

    public function __construct(
        ShelfPresenter $shelfPresenter,
        User $user,
        UuidInterface $shelfUuid,
        ?Users $newManagers,
        ?Users $oldManagers,
        ?Users $newUsers,
        ?Users $oldUsers
    ) {
        $this->shelfPresenter = $shelfPresenter;
        $this->user = $user;
        $this->shelfUuid = $shelfUuid;
        $this->newManagers = $newManagers;
        $this->oldManagers = $oldManagers;
        $this->newUsers = $newUsers;
        $this->oldUsers = $oldUsers;
    }

    public function getResultsHolder(): ShelfPresenter
    {
        return $this->shelfPresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getShelfUuid(): UuidInterface
    {
        return $this->shelfUuid;
    }

    public function getNewManagers(): ?Users
    {
        return $this->newManagers;
    }

    public function getOldManagers(): ?Users
    {
        return $this->oldManagers;
    }

    public function getNewUsers(): ?Users
    {
        return $this->newUsers;
    }

    public function getOldUsers(): ?Users
    {
        return $this->oldUsers;
    }
}
