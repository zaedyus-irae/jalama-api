<?php

namespace ZI\Jalama\Domain\Game\Actions\Exceptions;

final class ActionTargetNotFoundException extends \Exception
{
}
