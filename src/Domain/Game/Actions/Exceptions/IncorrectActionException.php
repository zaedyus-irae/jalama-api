<?php

namespace ZI\Jalama\Domain\Game\Actions\Exceptions;

final class IncorrectActionException extends \Exception
{
}
