<?php

namespace ZI\Jalama\Domain\Game\Actions\Exceptions;

final class UnauthorizedActionException extends \Exception
{
}
