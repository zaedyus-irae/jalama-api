<?php

namespace ZI\Jalama\Domain\Game\Actions\Listing;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Game\Model\Author;
use ZI\Jalama\Domain\Shared\Actions\Listing\PaginatedSetTrait;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedSetTraits;

final class PaginatedAuthors extends Set
{
    use UniquelyIdentifiedSetTraits;
    use PaginatedSetTrait;

    public function __construct(
        Pagination $pagination,
        int $totalNumberOfElements,
        Author ...$authors
    ) {
        $this->pagination = $pagination;
        $this->totalNumberOfElements = $totalNumberOfElements;

        parent::__construct(Author::class, $authors);
    }
}
