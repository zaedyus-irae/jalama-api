<?php

namespace ZI\Jalama\Domain\Game\Actions\Listing;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Game\Model\Shelf;
use ZI\Jalama\Domain\Shared\Actions\Listing\PaginatedSetTrait;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedSetTraits;

final class PaginatedShelves extends Set
{
    use PaginatedSetTrait;
    use UniquelyIdentifiedSetTraits;

    public function __construct(
        Pagination $pagination,
        int $totalNumberOfElements,
        Shelf ...$shelves
    ) {
        $this->pagination = $pagination;
        $this->totalNumberOfElements = $totalNumberOfElements;

        parent::__construct(Shelf::class, $shelves);
    }
}
