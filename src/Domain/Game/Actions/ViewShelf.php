<?php

namespace ZI\Jalama\Domain\Game\Actions;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\Outcomes\ShelfPresenter;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;

final class ViewShelf implements ReadingActionInterface, UserActionInterface, ShelfActionInterface
{
    private ShelfPresenter $shelfPresenter;
    private User $user;
    private UuidInterface $shelfUuid;

    public function __construct(
        ShelfPresenter $shelfPresenter,
        User $user,
        UuidInterface $shelfUuid
    ) {
        $this->shelfPresenter = $shelfPresenter;
        $this->user = $user;
        $this->shelfUuid = $shelfUuid;
    }

    public function getResultsHolder(): ShelfPresenter
    {
        return $this->shelfPresenter;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getShelfUuid(): UuidInterface
    {
        return $this->shelfUuid;
    }
}
