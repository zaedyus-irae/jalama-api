<?php

namespace ZI\Jalama\Domain\Game\Model;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedInterface;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedTrait;

final class Publisher implements UniquelyIdentifiedInterface
{
    use UniquelyIdentifiedTrait;

    private string $name;

    private function __construct(UuidInterface $uuid, string $name)
    {
        $this->uuid = $uuid;
        $this->name = $name;
    }

    public static function new(string $name): self
    {
        return new self(self::getRandomUuid(), $name);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
