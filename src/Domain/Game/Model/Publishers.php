<?php

namespace ZI\Jalama\Domain\Game\Model;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedSetTraits;

final class Publishers extends Set
{
    use UniquelyIdentifiedSetTraits;

    public function __construct(Publisher ...$publishers)
    {
        parent::__construct(Publisher::class, $publishers);
    }
}
