<?php

namespace ZI\Jalama\Domain\Game\Model;

final class PlayerRequirement
{
    private ?int $minNumber;
    private ?int $maxNumber;

    public function __construct(?int $minNumber, ?int $maxNumber)
    {
        if ($minNumber !== null && $minNumber <= 0) {
            throw new \InvalidArgumentException('Minimum player should be a positive integer, or null.');
        }
        if ($maxNumber !== null && $maxNumber <= 0) {
            throw new \InvalidArgumentException('Maximum player should be a positive integer, or null.');
        }
        if ($maxNumber !== null && $maxNumber < $minNumber) {
            throw new \InvalidArgumentException('Maximum player should not be lower than minimum player.');
        }

        $this->minNumber = $minNumber;
        $this->maxNumber = $maxNumber;
    }

    public function getMinNumber(): ?int
    {
        return $this->minNumber;
    }

    public function getMaxNumber(): ?int
    {
        return $this->maxNumber;
    }
}
