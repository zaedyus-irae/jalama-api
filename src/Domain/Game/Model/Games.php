<?php

namespace ZI\Jalama\Domain\Game\Model;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedSetTraits;

final class Games extends Set
{
    use UniquelyIdentifiedSetTraits;

    public function __construct(Game ...$games)
    {
        parent::__construct(Game::class, $games);
    }
}
