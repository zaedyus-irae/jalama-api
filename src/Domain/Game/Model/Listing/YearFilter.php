<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

final class YearFilter
{
    private ?int $after;
    private ?int $before;

    public function __construct(?int $after, ?int $before)
    {
        if ($before !== null && $before < $after) {
            throw new \InvalidArgumentException('To search a period, before must be greater than after.');
        }

        $this->after = $after;
        $this->before = $before;
    }

    public function getAfter(): ?int
    {
        return $this->after;
    }

    public function getBefore(): ?int
    {
        return $this->before;
    }
}
