<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

final class ShelvesFilters
{
    private UserFilter $userFilter;
    private ?string $text;

    public function __construct(
        UserFilter $userFilter,
        ?string $text
    ) {
        $this->userFilter = $userFilter;
        $this->text = $text;
    }

    public function getUserFilter(): UserFilter
    {
        return $this->userFilter;
    }

    public function getText(): ?string
    {
        return $this->text;
    }
}
