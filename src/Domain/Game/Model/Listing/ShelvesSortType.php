<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<ShelvesSortType>
 *
 * @method static self ACCESS()
 * @method static self NAME()
 */
final class ShelvesSortType extends Enum
{
    public const ACCESS = 'access';
    public const NAME = 'name';
}
