<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<GamesSortType>
 *
 * @method static self DURATION()
 * @method static self NAME()
 * @method static self PLAYERS()
 * @method static self RANDOM()
 * @method static self YEAR()
 */
final class GamesSortType extends Enum
{
    public const DURATION = 'duration';
    public const NAME = 'name';
    public const PLAYERS = 'players';
    public const RANDOM = 'random';
    public const YEAR = 'year';
}
