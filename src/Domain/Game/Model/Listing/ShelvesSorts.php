<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Shared\UniquelyEnumSetTraits;

final class ShelvesSorts extends Set
{
    use UniquelyEnumSetTraits;

    public function __construct(ShelvesSortType ...$shelvesSort)
    {
        parent::__construct(ShelvesSortType::class, $shelvesSort);
    }
}
