<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

use ZI\Jalama\Domain\Game\Model\Authors;
use ZI\Jalama\Domain\Game\Model\DurationRequirement;
use ZI\Jalama\Domain\Game\Model\PlayerRequirement;
use ZI\Jalama\Domain\Game\Model\Publishers;
use ZI\Jalama\Domain\Game\Model\Shelves;

final class GamesFilters
{
    private UserFilter $userFilter;
    private ?Shelves $shelves;
    private ?string $text;
    private ?PlayerRequirement $playerRequirement;
    private ?DurationRequirement $durationRequirement;
    private ?Authors $authors;
    private ?Publishers $publishers;
    private ?YearFilter $yearFilter;

    public function __construct(
        UserFilter $userFilter,
        ?Shelves $shelves,
        ?string $text,
        ?PlayerRequirement $playerRequirement,
        ?DurationRequirement $durationRequirement,
        ?Authors $authors,
        ?Publishers $publishers,
        ?YearFilter $yearFilter
    ) {
        $this->userFilter = $userFilter;
        $this->shelves = $shelves;
        $this->text = $text;
        $this->playerRequirement = $playerRequirement;
        $this->durationRequirement = $durationRequirement;
        $this->authors = $authors;
        $this->publishers = $publishers;
        $this->yearFilter = $yearFilter;
    }

    public function getUserFilter(): UserFilter
    {
        return $this->userFilter;
    }

    public function getShelves(): ?Shelves
    {
        return $this->shelves;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function getPlayerRequirement(): ?PlayerRequirement
    {
        return $this->playerRequirement;
    }

    public function getDurationRequirement(): ?DurationRequirement
    {
        return $this->durationRequirement;
    }

    public function getAuthors(): ?Authors
    {
        return $this->authors;
    }

    public function getPublishers(): ?Publishers
    {
        return $this->publishers;
    }

    public function getYearFilter(): ?YearFilter
    {
        return $this->yearFilter;
    }
}
