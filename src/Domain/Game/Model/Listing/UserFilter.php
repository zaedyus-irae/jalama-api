<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

use ZI\Jalama\Domain\Account\Model\User;

final class UserFilter
{
    private User $user;
    private UserFilterType $userFilterType;

    public function __construct(User $user, UserFilterType $userFilterType)
    {
        $this->user = $user;
        $this->userFilterType = $userFilterType;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getUserFilterType(): UserFilterType
    {
        return $this->userFilterType;
    }
}
