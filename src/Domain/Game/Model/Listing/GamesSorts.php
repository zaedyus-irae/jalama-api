<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Shared\UniquelyEnumSetTraits;

final class GamesSorts extends Set
{
    use UniquelyEnumSetTraits;

    public function __construct(GamesSortType ...$gamesSort)
    {
        parent::__construct(GamesSortType::class, $gamesSort);
    }
}
