<?php

namespace ZI\Jalama\Domain\Game\Model\Listing;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<UserFilterType>
 *
 * @method static self OWNER_ONLY()
 * @method static self ALL_VIEWABLE()
 */
final class UserFilterType extends Enum
{
    public const OWNER_ONLY = 'owner';
    public const ALL_VIEWABLE = 'access';
}
