<?php

namespace ZI\Jalama\Domain\Game\Model;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedSetTraits;

final class Authors extends Set
{
    use UniquelyIdentifiedSetTraits;

    public function __construct(Author ...$authors)
    {
        parent::__construct(Author::class, $authors);
    }
}
