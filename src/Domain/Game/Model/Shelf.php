<?php

namespace ZI\Jalama\Domain\Game\Model;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Account\Model\Users;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedInterface;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedTrait;

final class Shelf implements UniquelyIdentifiedInterface
{
    use UniquelyIdentifiedTrait;

    private User $owner;
    private string $name;
    private string $description;
    private ?string $cover;
    private Users $managers;
    private Users $users;

    private function __construct(UuidInterface $uuid, User $owner, string $name)
    {
        $this->uuid = $uuid;
        $this->owner = $owner;
        $this->name = $name;
        $this->description = '';
        $this->cover = null;
        $this->managers = new Users();
        $this->users = new Users();

        $this->managers->add($owner);
        $this->users->add($owner);
    }

    public static function new(User $owner, string $name): self
    {
        return new self(self::getRandomUuid(), $owner, $name);
    }

    public function getOwner(): User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): void
    {
        $this->owner = $owner;

        $this->managers->add($owner);
        $this->users->add($owner);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): void
    {
        $this->cover = $cover;
    }

    /**
     * @return Users|User[]
     */
    public function getManagers(): Users
    {
        return $this->managers;
    }

    public function setManagers(Users $managers): void
    {
        if (!$managers->contains($this->owner)) {
            throw new \InvalidArgumentException('Cannot remove the owner from the managers.');
        }

        $this->managers = $managers;

        foreach ($this->managers as $manager) {
            $this->users->add($manager);
        }
    }

    /**
     * @return Users|User[]
     */
    public function getUsers(): Users
    {
        return $this->users;
    }

    public function setUsers(Users $users): void
    {
        if (!$users->contains($this->owner)) {
            throw new \InvalidArgumentException('Cannot remove the owner from the users.');
        }
        foreach ($this->managers as $manager) {
            if (!$users->contains($manager)) {
                throw new \InvalidArgumentException('Cannot remove a manager from the users.');
            }
        }

        $this->users = $users;
    }
}
