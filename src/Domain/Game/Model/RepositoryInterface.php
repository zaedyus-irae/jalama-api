<?php

namespace ZI\Jalama\Domain\Game\Model;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Game\Model\Listing\AuthorsFilters;
use ZI\Jalama\Domain\Game\Model\Listing\GamesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\GamesSorts;
use ZI\Jalama\Domain\Game\Model\Listing\PublishersFilters;
use ZI\Jalama\Domain\Game\Model\Listing\ShelvesFilters;
use ZI\Jalama\Domain\Game\Model\Listing\ShelvesSorts;
use ZI\Jalama\Domain\Shared\Model\Listing\Pagination;

interface RepositoryInterface
{
    public function saveShelf(Shelf $shelf): void;

    public function getShelf(UuidInterface $shelfUuid): ?Shelf;

    public function deleteShelf(UuidInterface $shelfUuid): void;

    public function findShelves(ShelvesFilters $shelvesFilters, ShelvesSorts $shelvesSorts, Pagination $pagination): Shelves;

    public function countShelves(ShelvesFilters $shelvesFilters): int;

    public function saveGame(Game $game): void;

    public function getGame(UuidInterface $gameUuid): ?Game;

    public function deleteGame(UuidInterface $gameUuid): void;

    public function findGames(GamesFilters $gamesFilters, GamesSorts $gamesSorts, Pagination $pagination): Games;

    public function countGames(GamesFilters $gamesFilters): int;

    public function findAuthors(AuthorsFilters $authorsFilters, Pagination $pagination): Authors;

    public function countAuthors(AuthorsFilters $authorsFilters): int;

    public function findPublishers(PublishersFilters $publishersFilters, Pagination $pagination): Publishers;

    public function countPublishers(PublishersFilters $publishersFilters): int;
}
