<?php

namespace ZI\Jalama\Domain\Game\Model;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedInterface;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedTrait;

final class Game implements UniquelyIdentifiedInterface
{
    use UniquelyIdentifiedTrait;

    private User $owner;
    private Shelf $shelf;
    private string $name;
    private string $description;
    private ?string $cover;
    private PlayerRequirement $playerRequirement;
    private DurationRequirement $durationRequirement;
    private Authors $authors;
    private Publishers $publishers;
    private ?int $year;
    private ?Game $baseGame;
    private ?string $boardGameGeekReference;

    private function __construct(UuidInterface $uuid, User $user, Shelf $shelf, string $name)
    {
        $this->uuid = $uuid;
        $this->owner = $user;
        $this->shelf = $shelf;
        $this->name = $name;
        $this->description = '';
        $this->cover = null;
        $this->playerRequirement = new PlayerRequirement(null, null);
        $this->durationRequirement = new DurationRequirement(null, null);
        $this->authors = new Authors();
        $this->publishers = new Publishers();
        $this->year = null;
        $this->baseGame = null;
        $this->boardGameGeekReference = null;
    }

    public static function new(User $user, Shelf $shelf, string $name): self
    {
        return new self(self::getRandomUuid(), $user, $shelf, $name);
    }

    public function getOwner(): User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): void
    {
        $this->owner = $owner;
    }

    public function getShelf(): Shelf
    {
        return $this->shelf;
    }

    public function setShelf(Shelf $shelf): void
    {
        $this->shelf = $shelf;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): void
    {
        $this->cover = $cover;
    }

    public function getPlayerRequirement(): PlayerRequirement
    {
        return $this->playerRequirement;
    }

    public function setPlayerRequirement(PlayerRequirement $playerRequirement): void
    {
        $this->playerRequirement = $playerRequirement;
    }

    public function getDurationRequirement(): DurationRequirement
    {
        return $this->durationRequirement;
    }

    public function setDurationRequirement(DurationRequirement $durationRequirement): void
    {
        $this->durationRequirement = $durationRequirement;
    }

    /**
     * @return Authors|Author[]
     */
    public function getAuthors(): Authors
    {
        return $this->authors;
    }

    public function setAuthors(Authors $authors): void
    {
        $this->authors = $authors;
    }

    /**
     * @return Publishers|Publisher[]
     */
    public function getPublishers(): Publishers
    {
        return $this->publishers;
    }

    public function setPublishers(Publishers $publishers): void
    {
        $this->publishers = $publishers;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): void
    {
        if ($year < 1800 || $year >= 3000) {
            throw new \InvalidArgumentException('This model only handle game between 1800 and 3000');
        }

        $this->year = $year;
    }

    public function getBaseGame(): ?self
    {
        return $this->baseGame;
    }

    public function setBaseGame(?self $baseGame): void
    {
        $this->baseGame = $baseGame;
    }

    public function getBoardGameGeekReference(): ?string
    {
        return $this->boardGameGeekReference;
    }

    public function setBoardGameGeekReference(?string $boardGameGeekReference): void
    {
        $this->boardGameGeekReference = $boardGameGeekReference;
    }
}
