<?php

namespace ZI\Jalama\Domain\Game\Model;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedSetTraits;

final class Shelves extends Set
{
    use UniquelyIdentifiedSetTraits;

    public function __construct(Shelf ...$shelves)
    {
        parent::__construct(Shelf::class, $shelves);
    }
}
