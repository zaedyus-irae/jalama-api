<?php

namespace ZI\Jalama\Domain\Game\Model;

final class DurationRequirement
{
    private ?int $minInSeconds;
    private ?int $maxInSeconds;

    public function __construct(?int $minInSeconds, ?int $maxInSeconds)
    {
        if ($minInSeconds !== null && $minInSeconds <= 0) {
            throw new \InvalidArgumentException('Minimum duration should be a positive integer, or null.');
        }
        if ($maxInSeconds !== null && $maxInSeconds <= 0) {
            throw new \InvalidArgumentException('Maximum duration should be a positive integer, or null.');
        }
        if ($maxInSeconds !== null && $maxInSeconds < $minInSeconds) {
            throw new \InvalidArgumentException('Maximum duration should not be lower than minimum duration.');
        }

        $this->minInSeconds = $minInSeconds;
        $this->maxInSeconds = $maxInSeconds;
    }

    public function getMinInSeconds(): ?int
    {
        return $this->minInSeconds;
    }

    public function getMaxInSeconds(): ?int
    {
        return $this->maxInSeconds;
    }
}
