<?php

namespace ZI\Jalama\Domain\Account\Model;

use Ramsey\Uuid\UuidInterface;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedInterface;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedTrait;

final class User implements UniquelyIdentifiedInterface
{
    use UniquelyIdentifiedTrait;

    private string $displayName;

    private function __construct(UuidInterface $uuid, string $displayName)
    {
        $this->uuid = $uuid;
        $this->displayName = $displayName;
    }

    public static function new(UuidInterface $uuid, string $displayName): self
    {
        return new self($uuid, $displayName);
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }
}
