<?php

namespace ZI\Jalama\Domain\Account\Model;

use Ramsey\Collection\Set;
use ZI\Jalama\Domain\Shared\UniquelyIdentifiedSetTraits;

class Users extends Set
{
    use UniquelyIdentifiedSetTraits;

    public function __construct(User ...$users)
    {
        parent::__construct(User::class, $users);
    }
}
