<?php

namespace ZI\Jalama\Domain\Account\Model;

interface RepositoryInterface
{
    public function saveUser(User $user): void;
}
