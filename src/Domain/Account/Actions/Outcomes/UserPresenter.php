<?php

namespace ZI\Jalama\Domain\Account\Actions\Outcomes;

use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Shared\Actions\Outcomes\ResultsHolder;

interface UserPresenter extends ResultsHolder
{
    public function present(User $result): void;
}
