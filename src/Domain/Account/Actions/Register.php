<?php

namespace ZI\Jalama\Domain\Account\Actions;

use ZI\Jalama\Domain\Account\Actions\Outcomes\UserPresenter;
use ZI\Jalama\Domain\Shared\Actions\ReadingActionInterface;
use ZI\Jalama\Domain\Shared\Actions\WritingActionInterface;

final class Register implements WritingActionInterface, ReadingActionInterface
{
    private UserPresenter $userPresenter;
    private string $displayName;
    private string $shelfName;

    public function __construct(
        UserPresenter $userPresenter,
        string $displayName,
        string $shelfName
    ) {
        $this->userPresenter = $userPresenter;
        $this->displayName = $displayName;
        $this->shelfName = $shelfName;
    }

    public function getResultsHolder(): UserPresenter
    {
        return $this->userPresenter;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function getShelfName(): string
    {
        return $this->shelfName;
    }
}
