<?php

namespace ZI\Jalama\Domain\Account\Actions\Handlers;

use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use ZI\Jalama\Domain\Account\Actions\Register;
use ZI\Jalama\Domain\Account\Model\RepositoryInterface;
use ZI\Jalama\Domain\Account\Model\User;
use ZI\Jalama\Domain\Game\Actions\CreateShelf;
use ZI\Jalama\Domain\Game\Actions\Outcomes\ShelfPresenter;
use ZI\JalamaTests\Domain\Shared\Actions\Outcomes\VoidPresenterTrait;

final class RegisterHandler
{
    private RepositoryInterface $repository;
    private string $uuidNamespace;
    private CommandBus $commandBus;

    public function __construct(RepositoryInterface $repository, string $uuidNamespace, CommandBus $commandBus)
    {
        $this->repository = $repository;
        $this->uuidNamespace = $uuidNamespace;
        $this->commandBus = $commandBus;
    }

    public function handle(Register $registerPayload): void
    {
        $user = User::new(
            Uuid::uuid5(Uuid::uuid5(Uuid::NIL, $this->uuidNamespace), $registerPayload->getDisplayName()),
            $registerPayload->getDisplayName()
        );

        $this->repository->saveUser($user);

        $this->commandBus->handle(
            new CreateShelf(
                new class() implements ShelfPresenter {
                    use VoidPresenterTrait;
                },
                $user,
                $registerPayload->getShelfName(),
                '',
                null
            )
        );

        $registerPayload->getResultsHolder()->present($user);
    }
}
